/*
 * CSocketManager.cpp
 *
 *  Created on: 03/05/2014
 *      Author: Rodrigo
 */

#include "CSocketManager.h"
#include "CSocketData.h"

#define FAIL_RATE 15 //15% of failing probability in a package
#define _PRINT_MESSAGES_OFF

CSocketManager::CSocketManager(SOCKET sock, unsigned int threadId) :
	m_uSocket(sock), m_iThreadId(threadId)
{

}

CSocketManager::~CSocketManager()
{
}

int CSocketManager::_GetLastError()
{
#ifdef __WINDOWS
	return WSAGetLastError();
#endif

#ifdef __LINUX
	return errno;
#endif
}

#include <time.h>
#include <ctime>

int CSocketManager::OnHandleConnection()
{
	cout << "    *OnHandleConnection: BEGIN"
	     << endl;

	COMM_STATUS bRet = STATUS_OK;
	srand (clock());

	clock_t t_begin = clock(), t_end = 0;

	float fElapsed;

	cout << "    *Data to be sent: ["
	     << m_sMessage << "]"
	     << " Start :" << t_begin
	     << endl;

//	int iTime = (rand() % 10) * 1000; //from [0 to 10s]
//
//	Sleep(iTime);
//
//	cout << "Esperei " << iTime << " segundos - " << m_sMessage << endl;

	//isso aqui deve ir na classe derivada
	int iBytesToSend = 0;
	int iBytesReceived=0;

	char* pLongBuffer = NULL;

	CSocketData* oDataFromClient = new CSocketData();
	CSocketData* oFirstFrameData = NULL; //info from First Frame goes here

	byte *pBuffer = new byte[CSocketData::GetFrameLength()];

	iBytesReceived = recv(m_uSocket, (char*) pBuffer, CSocketData::GetFrameLength(), 0);

	#ifdef _PRINT_MESSAGES_ON
		cout << "    *Receiving data - From Client ="
			 << iBytesReceived << "bytes. To Client="
			 << CSocketData::GetFrameLength()
			 << " bytes."
			 << endl;
	#endif

    if( iBytesReceived == SOCKET_ERROR || iBytesReceived != CSocketData::GetFrameLength() )
    {
		#ifdef _PRINT_MESSAGES_ON
    		cout << "    *Error in Receiving: "
    			 << _GetLastError()
    			 << endl;
		#endif
    	bRet = STATUS_RECEIVE_ERROR;
    	goto FINISH; //nao pode fazer isso... trava o cliente... manda um retorno de erro pra ele
    }
    else
    {
        //OnDataReceived(); //seria bom criar um metodo desse nesta classe ou em uma derivada

    	//Check if this is the first frame which has arrived...
    	if( oFirstFrameData == NULL )
    		oFirstFrameData = new CSocketData( (SOCK_DATA_61850*) pBuffer );
    	oDataFromClient->SetFrame(pBuffer, iBytesReceived); //copy frame from temp buffer to oDataFromClient object

		//Allocate Long Buffer for the other frames
    	pLongBuffer = new char[oFirstFrameData->GetTotalBytes()];
		memset(pLongBuffer, 0x00, oFirstFrameData->GetTotalBytes());

//		double* dbData = oData->GetData();
//		cout << "Dados[0]: " << dbData[0] << endl;
//		cout << "Dados[1]: " << dbData[1] << endl;
//		cout << "Dados[2]: " << dbData[2] << endl;
//		cout << "Dados[3]: " << dbData[3] << endl;
//		cout << "Dados[4]: " << dbData[4] << endl;
//		cout << "Dados[5]: " << dbData[5] << endl;
//		cout << "Dados[6]: " << dbData[6] << endl;
//		cout << "Dados[7]: " << dbData[7] << endl;
//		cout << "Dados[8]: " << dbData[8] << endl;
//		cout << "Dados[9]: " << dbData[9] << endl;
//		cout << "Dados[10]: " << dbData[10] << endl;
//		cout << "Dados[11]: " << dbData[11] << endl;
//		cout << "Dados[12]: " << dbData[12] << endl;

		//Treat command - n�o vai precisar tratar comando aqui, este ser� basicamente o tratamento do PRIMEIRO FRAME
		if( oFirstFrameData->GetCommand() == COMMAND_HELLO )
		{
			#ifdef _PRINT_MESSAGES_ON
				cout << "    *Request for Hello from client on "
					 << m_sMessage
					 << endl;
			#endif
			if( oFirstFrameData->GetNumberOfFrames() > 1 )
			{
				//Copy data to Long buffer
				int iOffSet = oDataFromClient->GetFrameIndex() * MAX_BUFFER;
				memcpy(pLongBuffer + iOffSet, oDataFromClient->GetData(), oDataFromClient->GetDataLength() );

				//Send return Data to Client
				int iTempCommand    = oDataFromClient->GetCommand();
				int iTempFrameIndex = oDataFromClient->GetFrameIndex();

				#ifdef _PRINT_MESSAGES_ON
					cout << "    *Frame info: Command= "
						 << oDataFromClient->GetCommand()
						 << " Frame "
						 << oDataFromClient->GetFrameIndex()
						 << " of "
						 << oDataFromClient->GetNumberOfFrames()
						 << endl;
				#endif
				oDataFromClient->ClearFrame(); //talvez, se n�o zerar o buffer e mudar apenas o status (ou minimo necessario), o cliente possa checar se os bytes recebidos foram de fato identicos aos enviados
				oDataFromClient->SetFrameStatus( STATUS_SERVER_ACK ); //Server acknowledge (received data correctly)
				oDataFromClient->SetFrameIndex( iTempFrameIndex );    //Set frame index which was received
				oDataFromClient->SetCommand( iTempCommand );          //Set command which was received

				//Transfer to buffer
				oDataFromClient->GetFrame( pBuffer, CSocketData::GetFrameLength() );

				iBytesToSend = 0;
				iBytesToSend = send( m_uSocket, (char*) pBuffer, CSocketData::GetFrameLength(), 0 ); //muda de windows pra linux

				if( iBytesToSend != CSocketData::GetFrameLength() )
				{
					#ifdef _PRINT_MESSAGES_ON
						cout << "    *Error in Sending: "
							 << _GetLastError()
							 << endl;
					#endif
					bRet = STATUS_SEND_ERROR;
				}
				else
				{
					bRet = STATUS_OK;
				}

				//FRAME TRANSFER LOOP
				//Este loop pode ser transformado num while que depende de uma contagem global dos frames (o indexador s� incrementa quando o frame for lido de forma bem sucedida)
				//em paralelo pode haver o "contador de qualidade de conex�o" (descrito mais abaixo) para cancelar uma conex�o que estiver solicitando muitos reenvios
				//(evita algum tipo de "ataque de requisi��o", por exemplo, cliente "malicioso" enviando frame truncado para for�ar solicita��o de reenvio pelo server).
				for(int i=0; i < oFirstFrameData->GetNumberOfFrames() - 1; ++i )
				{
					//Get the other frames
					//Get next Frame from Client

					iBytesReceived = recv(m_uSocket, (char*) pBuffer, CSocketData::GetFrameLength(), 0);

					#ifdef _PRINT_MESSAGES_ON
						cout << "    *Receiving data - From Client ="
							 << iBytesReceived
							 << " bytes. To Client="
							 << CSocketData::GetFrameLength()
							 << " bytes."
							 << endl;
					#endif

				    if( iBytesReceived == SOCKET_ERROR || iBytesReceived != oDataFromClient->GetFrameLength() )
				    {
						#ifdef _PRINT_MESSAGES_ON
							cout << "    *Error in Receiving: "
								 << _GetLastError()
								 << endl;
						#endif

				        bRet = STATUS_RECEIVE_ERROR; //� poss�vel fazer um controle melhor? afinal o cliente pode tentar REENVIAR o frame perdido (mas como os bytes n�o vieram corretamente, o parsing da estrutura foi comprometido)
				                                     //eventualmente, se o numero de bytes for tal que seja possivel identificar o comando e o frameindex, � poss�vel tentar uma solicita��o de reenvio
				                                     //porem estas tentativas deveriam ser contabilizadas e se, forem feitas muitas tentativas, o servidor poderia abortar a transferencia por "m� qualidade na conex�o"
				                                     //Na pr�tica, � basicamente o tratamento simulado para falha, abaixo. Poderia ser colocado aqui.
				        goto FINISH;
				    }
				    else
				    {
				    	oDataFromClient->SetFrame(pBuffer, iBytesReceived);

				    	//Take decision: accept or reject the frame? (Fail simulation)
				    	//FAIL RATE SIMULATION - BEGIN
				    	if( rand() % 100 <= FAIL_RATE )
				    	{
							#ifdef _PRINT_MESSAGES_ON
								cout << "    *FAILED! Frame info: Command= "
									 << oDataFromClient->GetCommand()
									 << " Frame "
									 << oDataFromClient->GetFrameIndex()
									 << " of "
									 << oDataFromClient->GetNumberOfFrames()
									 << "("
									 << oDataFromClient->GetDataLength()
									 << ") bytes of value data."
									 << endl;
							#endif

				    		//Send response to client
							int iTempCommand    = oDataFromClient->GetCommand();
							int iTempFrameIndex = oDataFromClient->GetFrameIndex();

							oDataFromClient->ClearFrame();
							oDataFromClient->SetFrameStatus( STATUS_DATA_CORRUPTED ); //Fail on packages
							oDataFromClient->SetFrameIndex( iTempFrameIndex );    //Set frame index which was received
							oDataFromClient->SetCommand( iTempCommand );          //Set command which was received

							//Transfer to buffer
							oDataFromClient->GetFrame(pBuffer, CSocketData::GetFrameLength());

							iBytesToSend = 0;

							iBytesToSend = send( m_uSocket, (char*) pBuffer, CSocketData::GetFrameLength(), 0 );

							if( iBytesToSend != CSocketData::GetFrameLength() )
							{
								#ifdef _PRINT_MESSAGES_ON
									cout << "    *Error in Sending: "
										 << _GetLastError()
										 << endl;
								#endif

								bRet = STATUS_SEND_ERROR;
							}
							else
							{
								bRet = STATUS_OK;
							}

							i--; //it's sync communication, the for loop will restore current index value
							continue;
				    	}//if( rand() % 100 <= FAIL_RATE )
				    	//FAIL RATE SIMULATION - END

				    	if( oDataFromClient->GetCommand() == oFirstFrameData->GetCommand() )
				    	{
				    		//Same command? Add data to long buffer

							//Append Frame on the LongBuffer
				    		iOffSet = oDataFromClient->GetFrameIndex() * MAX_BUFFER;
				    		memcpy(pLongBuffer + iOffSet, oDataFromClient->GetData(), oDataFromClient->GetDataLength() );

							#ifdef _PRINT_MESSAGES_ON
								cout << "    *Frame info: Command= "
									 << oDataFromClient->GetCommand()
									 << " Frame "
									 << oDataFromClient->GetFrameIndex()
									 << " of "
									 << oDataFromClient->GetNumberOfFrames()
									 << "("
									 << oDataFromClient->GetDataLength()
									 << ") bytes of value data."
									 << endl;
							#endif

				    		//Send response to client
							int iTempCommand    = oDataFromClient->GetCommand();
							int iTempFrameIndex = oDataFromClient->GetFrameIndex();

							oDataFromClient->ClearFrame();
							oDataFromClient->SetFrameStatus( STATUS_SERVER_ACK ); //Server acknowledge (received data correctly)
							oDataFromClient->SetFrameIndex( iTempFrameIndex );    //Set frame index which was received
							oDataFromClient->SetCommand( iTempCommand );          //Set command which was received

							//Transfer to buffer
							oDataFromClient->GetFrame(pBuffer, CSocketData::GetFrameLength());

							iBytesToSend = 0;

							iBytesToSend = send( m_uSocket, (char*) pBuffer, CSocketData::GetFrameLength(), 0 );

							if( iBytesToSend != CSocketData::GetFrameLength() )
							{
								#ifdef _PRINT_MESSAGES_ON
									cout << "    *Error in Sending: "
										 << _GetLastError()
										 << endl;
								#endif

								bRet = STATUS_SEND_ERROR;
							}
							else
							{
								bRet = STATUS_OK;
							}
				    	}//if( oData->GetCommand() == COMMAND_HELLO )
				    }//else...if( iBytesReceived == ...)
				}//for(i)
			}//if( oData->GetNumberOfFrames() > 1 )

			//Elapsed time for Transmission
			t_end = clock();

			cout << "End : " << t_end << endl;
			//CLOCKS_PER_SEC (CPS): on Windows, CPS = 1000, on BBB CPS = 1000000
			fElapsed = ((float)(t_end - t_begin)) / CLOCKS_PER_SEC * 1000;

			//printf("CPS = %ld\n", CLOCKS_PER_SEC);

			//If we have gotten here, Transmission has ended, let's print the LongBuffer
			//For debug printing on screen, we need to add the '\0' in the end of buffer
			cout << "Long Buffer is ["
				 << oFirstFrameData->GetTotalBytes()
				 << "] bytes Long"
				 << endl;

			if(oFirstFrameData->GetFrameType() == TYPE_INT_SEQUENCE) //este tipo de informa��o pode ser tratado por uma classe derivada, uma vez que o TRANSFER LOOP ja terminou
			{
				cout << "Type Int Sequence"
					 << endl;

				long lIntSequenceLength = oFirstFrameData->GetFrameTypeLength();

				int* pDebugBuffer = new int[lIntSequenceLength];
				memset(pDebugBuffer, 0x00, lIntSequenceLength * sizeof(int));//se houver diferen�a no sizeof do cliente em rela��o ao servidor, teremos problemas (lembrando que podem ser arquiteturas diferentes)
				                                                             //a vantagem de ter este campo pode ser, a priori, justamente para VALIDAR O MARSHALING (as duas pontas Cliente/Servidor devem ter o mesmo tamanho neste campo)
				                                                             //claro que a organiza��o numerica (endianess ou outra formata��o) podem interferir no parsing, mas a� podemos cair em "casos especiais". Deve ser mencionado na tese, mas tratar isso agora daria trabalho excessivo.
				memcpy(pDebugBuffer, pLongBuffer, oFirstFrameData->GetTotalBytes());

				cout << "Sizeof(int) = "
					 << sizeof(int)
					 << " size = "
					 << oFirstFrameData->GetTotalBytes()
					 << endl;

				FILE* fp = fopen("FromClient.txt", "w");

				for(int i=0; i < lIntSequenceLength; ++i)
				{
					fprintf(fp, "%d\n", pDebugBuffer[i]);
				}
				//fwrite(pLongBuffer, 1, oFirstFrameData->GetTotalBytes(), fp);
				fclose(fp);

				fp = fopen("FromClientBinary.txt", "wb");
				fwrite(pLongBuffer, 1, oFirstFrameData->GetTotalBytes(), fp);
				fclose(fp);

				delete[] pDebugBuffer;
			}

			if(oFirstFrameData->GetFrameType() == TYPE_TEXT) //este tipo de informa��o pode ser tratado por uma classe derivada, uma vez que o TRANSFER LOOP ja terminou
			{
				cout << "Type Text"
					 << endl;

				char* pDebugBuffer = new char[oFirstFrameData->GetTotalBytes() + 1];
				memset(pDebugBuffer, 0x00, oFirstFrameData->GetTotalBytes() + 1);
				memcpy(pDebugBuffer, pLongBuffer, oFirstFrameData->GetTotalBytes());

				ofstream oFile;
				oFile.open ("FromClient.txt");
				oFile << pDebugBuffer;
				oFile.close();
				delete[] pDebugBuffer;
			}
		}//if( oFirstFrameData->GetCommand() == COMMAND_HELLO )

		cout << "    *Returning..."
			 << endl;
    }

    FINISH:

    delete[] pBuffer;
    if(pLongBuffer)
    	delete[] pLongBuffer;

    delete oDataFromClient;
    delete oFirstFrameData;

	cout << "    *Communication with server: END - Status: "
		 << CSocketData::GetCommStatusText(bRet)
		 << " Elapsed Time(ms): "
		 << fElapsed
		 << endl;

//	t_begin = clock();
//	Sleep(1500);
//	t_end = clock();
//
//	cout << "Begin2:" << t_begin << " End 2: " << t_end << "Elapsed = " << t_end - t_begin << endl;

	return bRet;
}

void CSocketManager::SetMessage( string sMessage )
{
	m_sMessage = sMessage;
}

void CSocketManager::CloseConnection()
{
#ifdef __WINDOWS
	shutdown(m_uSocket, 0x02/*SD_BOTH*/);
	closesocket(m_uSocket);
#endif

#ifdef __LINUX
	close(m_uSocket);
#endif

}

SOCKET CSocketManager::GetSocket()
{
	return m_uSocket;
}
