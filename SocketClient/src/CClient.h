/*
 * CClient.h
 *
 *  Created on: 01/05/2014
 *      Author: Rodrigo T. Caropreso
 */

#ifndef CCLIENT_H_
#define CCLIENT_H_

#include <string.h>
#include <sstream>
#include <iostream>
#include <vector>

#ifdef __WINDOWS
	#include <process.h>
	#include <windows.h>
	#include "winsock2.h"
    #include <pthread.h>
#endif

#ifdef __LINUX
	#include <errno.h>
	#include <arpa\inet.h>
	#include <pthread.h>
	#include <stdio.h>
	#include <arm-linux-gnueabi/sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
#endif

#ifdef __LINUX
	typedef unsigned int SOCKET;
#endif

#include "CSocketData.h"
#include "CSocketClientBase.h"

using namespace std;

class CSocketData;

class CClient : CSocketClientBase
{
public:
	CClient(string sIP, long lPort);
	virtual ~CClient();

	virtual COMM_STATUS ConnectToServer();
	virtual void SetData(CSocketData* oData);
	virtual COMM_STATUS CommunicateToServer();
	virtual CSocketData* GetData();

	string CommStatusText(COMM_STATUS Result);

private:
	int _GetLastError();
	COMM_STATUS ConnectToServerForWindows();
	COMM_STATUS ConnectToServerForLinux();
};

#endif /* CCLIENT_H_ */
