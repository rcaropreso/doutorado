#ifndef CHASHPROXY_H
#define CHASHPROXY_H

#include <string.h>
#include <openssl/md5.h> //test for OpenSSLLib
#include <openssl/evp.h>

#define DIGEST_SIZE_BYTES_SHA256 64

class CHashProxy
{
public:
    CHashProxy();
    bool GenerateSHA256(const unsigned char* strPlainText, unsigned int iPlainTextLength);
    bool GetDigestSHA256(unsigned char* strHashCode);
    int GetAllocationSizeForSHA256();

    bool GenerateMD5(const unsigned char* strPlainText, unsigned int iPlainTextLength);
    bool GetDigestMD5(unsigned char* strHashCode);
    int GetAllocationSizeForMD5();

private:
    unsigned char m_DigestSHA256[EVP_MAX_MD_SIZE];
    unsigned char m_DigestMD5[MD5_DIGEST_LENGTH];
};

#endif // CHASHPROXY_H
