#ifndef CHASHUNITTEST_H
#define CHASHUNITTEST_H

#include "CHashProxy.h"
#include "CSecurityCommonDefs.h"

class CHashUnitTest
{
public:
    CHashUnitTest();
    CHashUnitTest(string strFileName);
    ~CHashUnitTest();

    void RunHashTest();

private:
    void LogOutput(char* strMessage, int iMessageLength);
    bool RunHashTestMD5();
    bool RunHashTestSHA256();

    _LOG_OUTPUT_TYPE m_LogType;
    FILE* m_fpLogFile;
};

#endif // CHASHUNITTEST_H
