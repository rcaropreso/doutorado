::Date: 2016/02/17
::Author: Rodrigo de Toledo Caropreso
::Build one Application from the Project

::@echo "Usage: buildOne [PROJECT_FOLDER] [all|DebugWin|DebugARM|ReleaseWin|ReleaseARM] [clean] [cleanmakefiles]"
:: %1 [PROJECT_FOLDER] -> full qualified path for current Project Component folder (i.e. "D:\_Doutorado\workspace\_SigProcessLibrary")
:: %2 all              -> Build current Project component for DebugWin, ReleaseWin, DebugARM and ReleaseARM targets
:: %2 clean            -> clean up all output folders for DebugWin, ReleaseWin, DebugARM and ReleaseARM targets
:: %2 cleanmakefiles   -> remove makefiles from 'src' folder (the makefiles remain on "makes" folder)

::example: buildOne "D:\_Doutorado\workspace\_SigProcessLibrary" DebugWin 

@echo off
::cls

set CURR_DIR=%CD%

set PROJECT_FOLDER="%1"

if '%1'=='' (
	goto showHelp)

if %2==cleanmakefiles (
	 set OPERATION=cleanmakefiles
	 goto cleanmakefiles
	 )

::Set the operation to be performed by 'make'
set OPERATION=%2

::Check for valid parameter
if %2==clean (
	 set OPERATION=clean
	 goto processOperation
	 )

if %2==all (
	 goto processOperation
	  )

if %2==DebugWin (
	 goto processOperation
	  )

if %2==ReleaseWin (
	 goto processOperation
	  )

if %2==DebugARM (
	 goto processOperation
	  )

if %2==ReleaseARM (
	 goto processOperation
	  )

::if we got here, the parameter is not valid
goto showHelp

:cleanmakefiles
@echo "Starting cleaning makefiles..."

@echo Cleaning %PROJECT_FOLDER%...
cd  /D %PROJECT_FOLDER%/src
del make*

@echo "Process has finished!"

goto END

:processOperation

@echo "Starting process..."

cd  /D %PROJECT_FOLDER%/src

copy makes\*.* /Y
call make %OPERATION%
echo.

::pause

@echo "Process has finished!"

goto END

:showHelp

echo.
echo %0
@echo ***This batch process performs one project component clean up and/or build***
@echo "Usage: buildOne [PROJECT_FOLDER] [all|DebugWin|DebugARM|ReleaseWin|ReleaseARM] [clean] [cleanmakefiles]"

:END
::go back to current directory
cd /D %CURR_DIR%
