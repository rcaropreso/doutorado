#include "CAesUnitTest.h"

CAesUnitTest::CAesUnitTest()
{
    m_LogType = OUT_SCREEN;
    m_fpLogFile = NULL;
}

CAesUnitTest::CAesUnitTest(string strFileName)
{
    m_LogType = OUT_FILE;
    m_fpLogFile = fopen(strFileName.c_str(), "w");

    if(m_fpLogFile == NULL)
    {
        cout << "Error opening log file!" << endl;
    }
}

CAesUnitTest::~CAesUnitTest()
{
    if(m_fpLogFile != NULL)
    {
        fclose(m_fpLogFile);
    }
}

void CAesUnitTest::LogOutput(char* strMessage, int iMessageLength)
{
    if(m_LogType == OUT_SCREEN)
    {
        cout << strMessage << endl;
    }
    else if(m_LogType == OUT_FILE)
    {
        fwrite(strMessage, 1, iMessageLength, m_fpLogFile);
    }
}

bool CAesUnitTest::RunAES128KeysTest(CAesProxy* oAES)
{
    stringstream ssMsg;

    //AES Test - BEGIN
    ssMsg << "Starting AES tests..." << endl;

    //*****Key Generation AES128*****
    ssMsg << "Generating AES128 Keys..." << endl;

    if( !oAES->GenerateKeys(AES128) )
    {
        ssMsg << "Error on AES128 GenerateKeys() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Send keys to Screen
    int iKeyLength = oAES->GetByteSizeOfKey(AES128);
    unsigned char* strKey = NULL;// = new unsigned char[iKeyLength];
    if( !oAES->GetKey(&strKey) )
    {
        ssMsg << "Error on AES128 - GetKey() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    string sText;
    char strAux[3];
    for(int i=0; i < iKeyLength; i++)
    {
        sprintf(strAux, "%02x", strKey[i]);
        sText.append(strAux);
    }

    ssMsg << "\nKey for AES128 is " << iKeyLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    int iIVLength = oAES->GetByteSizeOfIV(AES128);
    unsigned char* strIV;// = new unsigned char[iIVLength];
    if( !oAES->GetInitialVector(&strIV) )
    {
        ssMsg << "Error on AES128 - GetInitialVector() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);
    for(int i=0; i < iIVLength; i++)
    {
        sprintf(strAux, "%02x", strIV[i]);
        sText.append(strAux);
    }

    ssMsg << "\nInitialVector for AES128 is " << iKeyLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strKey;
    delete[] strIV;

    return true;
}

bool CAesUnitTest::RunAES128EncryptionTest(CAesProxy* oAES)
{
    stringstream ssMsg;

    unsigned char strPlainText[8] = "Rodrigo";

    //*****Encryption Test AES128*****
    ssMsg << "\nEncryption test (AES128) for \'Rodrigo\'..." << endl;

    unsigned char* strCypheredText = NULL;
    unsigned int iCypheredTextLength = 0;
    if( !oAES->Encrypt(strPlainText, &strCypheredText, &iCypheredTextLength) ) //strPlainText contains 'Rodrigo' initialized on the begginning of the function
    {
        ssMsg << "Encryption test (AES128) for \'Rodrigo\' FAILED! Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    string sText;
    char strAux[3];
    for(unsigned int i=0; i < iCypheredTextLength; i++)
    {
        sprintf(strAux, "%02x", strCypheredText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nCyphered text for AES128 is " << iCypheredTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    //*****Decryption Test AES128*****
    ssMsg << "\nDecryption test (AES128) for '" << sText << "'..." << endl;

    unsigned char* strDecodedText = NULL;
    unsigned int iDecodedTextLength = 0;
    if( !oAES->Decrypt(strCypheredText, iCypheredTextLength, &strDecodedText, &iDecodedTextLength) )
    {
        ssMsg << "Decryption test (AES128) for \'Rodrigo\' FAILED! Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);

    for(unsigned int i=0; i < iDecodedTextLength; i++)
    {
        sprintf(strAux, "%c", strDecodedText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nDecoded text for AES128 is " << iDecodedTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strCypheredText;
    delete[] strDecodedText;

    return true;
 }

bool CAesUnitTest::RunAES256KeysTest(CAesProxy* oAES)
{
    stringstream ssMsg;

    //AES Test - BEGIN
    ssMsg << "\nStarting AES tests..." << endl;

    //*****Key Generation AES256*****
    ssMsg << "Generating AES256 Keys..." << endl;

    if( !oAES->GenerateKeys(AES256) )
    {
        ssMsg << "Error on AES256 GenerateKeys() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Send keys to Screen
    int iKeyLength = oAES->GetByteSizeOfKey(AES256);
    unsigned char* strKey = NULL;// = new unsigned char[iKeyLength];
    if( !oAES->GetKey(&strKey) )
    {
        ssMsg << "Error on AES256 - GetKey() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    string sText;
    char strAux[3];
    for(int i=0; i < iKeyLength; i++)
    {
        sprintf(strAux, "%02x", strKey[i]);
        sText.append(strAux);
    }

    ssMsg << "\nKey for AES256 is " << iKeyLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    int iIVLength = oAES->GetByteSizeOfIV(AES256);
    unsigned char* strIV;// = new unsigned char[iIVLength];
    if( !oAES->GetInitialVector(&strIV) )
    {
        ssMsg << "Error on AES256 - GetInitialVector() operation. Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);
    for(int i=0; i < iIVLength; i++)
    {
        sprintf(strAux, "%02x", strIV[i]);
        sText.append(strAux);
    }

    ssMsg << "\nInitialVector for AES256 is " << iKeyLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strKey;
    delete[] strIV;

    return true;
}

bool CAesUnitTest::RunAES256EncryptionTest(CAesProxy* oAES)
{
    stringstream ssMsg;

    unsigned char strPlainText[8] = "Rodrigo";

    //*****Encryption Test AES256*****
    ssMsg << "\nEncryption test (AES256) for \'Rodrigo\'..." << endl;

    unsigned char* strCypheredText = NULL;
    unsigned int iCypheredTextLength = 0;
    if( !oAES->Encrypt(strPlainText, &strCypheredText, &iCypheredTextLength) ) //strPlainText contains 'Rodrigo' initialized on the begginning of the function
    {
        ssMsg << "Encryption test (AES256) for \'Rodrigo\' FAILED! Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    string sText;
    char strAux[3];
    for(unsigned int i=0; i < iCypheredTextLength; i++)
    {
        sprintf(strAux, "%02x", strCypheredText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nCyphered text for AES256 is " << iCypheredTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    //*****Decryption Test AES256*****
    ssMsg << "\nDecryption test (AES256) for '" << sText << "'..." << endl;

    unsigned char* strDecodedText = NULL;
    unsigned int iDecodedTextLength = 0;
    if( !oAES->Decrypt(strCypheredText, iCypheredTextLength, &strDecodedText, &iDecodedTextLength) )
    {
        ssMsg << "Decryption test (AES256) for \'Rodrigo\' FAILED! Error Code [" << oAES->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);

    for(unsigned int i=0; i < iDecodedTextLength; i++)
    {
        sprintf(strAux, "%c", strDecodedText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nDecoded text for AES256 is " << iDecodedTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strCypheredText;
    delete[] strDecodedText;

    return true;
 }

void CAesUnitTest::RunAesTest()
{
    //AES128
    CAesProxy* oAES = new CAesProxy();
    RunAES128KeysTest(oAES);
    RunAES128EncryptionTest(oAES);
    delete oAES;

    //AES256
    oAES = new CAesProxy();
    RunAES256KeysTest(oAES);
    RunAES256EncryptionTest(oAES);
    delete oAES;
}
