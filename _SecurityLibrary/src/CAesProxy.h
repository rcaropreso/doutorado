#ifndef CAESPROXY_H
#define CAESPROXY_H

#include <string.h>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/conf.h>

#define KEY_LENGTH_AES256 32
#define IV_LENGTH_AES256  16

#define KEY_LENGTH_AES128 16
#define IV_LENGTH_AES128  16

enum _ERROR_TYPE
{
    NO_ERROR_REPORTED,
    ENCRYPTION_ERROR,
    DECRYPTION_ERROR,
    INVALID_PARAMETER,
    ENCRYPTION_TYPE_NOT_DEFINED,
    KEY_GENERATION_ERROR,
    MEMORY_ALLOCATION_ERROR,
    INSUFFICIENT_BUFFER_SIZE,
    NO_KEY_DEFINED
};

enum _ENC_TYPE
{
    AES128,
    AES256
};

class CAesProxy
{
public:
    CAesProxy();
    CAesProxy(_ENC_TYPE encryptionType, unsigned char* const strKey,
              unsigned char* const strInitialVector);

    ~CAesProxy();

    bool GenerateKeys(_ENC_TYPE encryptionType = AES128);

    _ENC_TYPE GetCurrentEncryptionType();

    bool GetKey(unsigned char** strKey);

    bool GetInitialVector(unsigned char** strIV);

    int GetByteSizeOfKey(_ENC_TYPE encryptionType);
    int GetByteSizeOfIV(_ENC_TYPE encryptionType);

    bool Encrypt(unsigned const char* const strPlainText,
                unsigned char** strCypheredText, unsigned int* iCypheredTextLength);

    bool Decrypt(unsigned const char* const strCypheredText, unsigned int iCypheredTextLength,
                unsigned char** strPlainText, unsigned int* iPlainTextLength);

    _ERROR_TYPE GetLastError();

private:
    void ReleaseMemory();
    bool AllocateKeys();

    unsigned char* m_strKey;
    unsigned char* m_strInitialVector;
    _ENC_TYPE m_CurrentEncryptionType;
    _ERROR_TYPE m_ErrorType;
};

#endif // CAESPROXY_H
