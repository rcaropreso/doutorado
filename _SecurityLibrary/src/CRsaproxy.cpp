#include "CRsaproxy.h"

#define IS_VALID_KEY_TYPE(Type) (Type == PRIVATE_KEY || Type == PUBLIC_KEY)

CRsaProxy::CRsaProxy()
{
    m_strPrivateKey = NULL;
    m_strPublicKey = NULL;

    m_iPrivateKeyLength = 0;
    m_iPublicKeyLength = 0;
}

CRsaProxy::~CRsaProxy()
{
    ReleaseMemory();
}

bool CRsaProxy::GenerateKeys(bool bSaveToFile, char* strFolder, char* strPrivateKeyFile, char* strPublicKeyFile)
{
    m_ErrorType = RSA_NO_ERROR;
    //Generate a Key-pair for RSA (private/public keys)
    RSA* keypair = RSA_generate_key(2048, 3, 0, 0);

    if(SetupKey(keypair, PUBLIC_KEY) == false)
    {
        m_ErrorType = RSA_KEY_GENERATION_ERROR;
        return false;
    }

    if(SetupKey(keypair, PRIVATE_KEY) == false)
    {
        m_ErrorType = RSA_KEY_GENERATION_ERROR;
        return false;
    }

    if(bSaveToFile)
    {
        if(!SaveKeysOnFile(strFolder, strPrivateKeyFile, strPublicKeyFile))
        {
            return false;
        }
    }

    //free objects
    RSA_free(keypair);

    return true;
}

bool CRsaProxy::SaveKeysOnFile(char* strFolder, char* strPrivateKeyFile, char* strPublicKeyFile)
{
    //Get time stamp
    time_t tNow = time(0);
    tm* tmLocal = localtime(&tNow);
    char strTimeStamp[16];
    strftime(strTimeStamp, sizeof(strTimeStamp), "%Y%m%d%H%M%S", tmLocal);

    //Build filename
    string sFolder(strFolder);
    if(strlen(strFolder) > 0 )
    {
    	printf("Monkey %d \n", strlen(strFolder));
    	sFolder += "\\";
    }

    string strPrivKeyFileName = strFolder + string("private_") + strTimeStamp + string(".pem");
    string strPublKeyFileName = strFolder + string("public_")  + strTimeStamp + string(".pem");

    //Save keys to file
    FILE* fp = fopen(strPrivKeyFileName.c_str(), "wb");

    if(fp == NULL )
    {
        m_ErrorType = RSA_OPENFILE_ERROR;
        return false;
    }

    fwrite(m_strPrivateKey, 1, m_iPrivateKeyLength, fp );
    fclose(fp);

    fp = fopen(strPublKeyFileName.c_str(), "wb");

    if(fp == NULL )
    {
        m_ErrorType = RSA_OPENFILE_ERROR;
        return false;
    }

    fwrite(m_strPublicKey, 1, m_iPublicKeyLength, fp );
    fclose(fp);

    if(strPrivateKeyFile != NULL)
    {
        strcpy(strPrivateKeyFile, strPrivKeyFileName.c_str()); //not validating overflow by now, beware on strPrivateKeyFile size
    }

    if(strPublicKeyFile != NULL)
    {
        strcpy(strPublicKeyFile, strPublKeyFileName.c_str()); //not validating overflow by now, beware on strPublicKeyFile size
    }

    return true;
}

bool CRsaProxy::LoadKeysFromFile(char* strFileName, _RSA_KEY_TYPE enKeyType)
{
    int iReturn = true;

    if(strFileName == NULL || !IS_VALID_KEY_TYPE(enKeyType))
    {
        m_ErrorType = RSA_INVALID_PARAMETER;
        return false;
    }

    RSA* rsa = RSA_new();
    FILE* fp = fopen(strFileName,"rb");

    if(fp == NULL)
    {
        m_ErrorType = RSA_OPENFILE_ERROR;
        iReturn = false;
        goto FINISH;
    }

    if(enKeyType == PUBLIC_KEY)
    {
        rsa = PEM_read_RSA_PUBKEY(fp, &rsa, NULL, NULL);

        if(SetupKey(rsa, PUBLIC_KEY) == false)
        {
            m_ErrorType = RSA_KEY_GENERATION_ERROR;
            iReturn = false;
            goto FINISH;
        }
    }
    else if(enKeyType == PRIVATE_KEY)
    {
        rsa = PEM_read_RSAPrivateKey(fp, &rsa, NULL, NULL);

        if(SetupKey(rsa, PRIVATE_KEY) == false)
        {
            m_ErrorType = RSA_KEY_GENERATION_ERROR;
            iReturn = false;
            goto FINISH;
        }
    }
    else
    {
        //unknown type
        m_ErrorType = RSA_KEY_TYPE_NOT_DEFINED;
        iReturn = false;
        goto FINISH;
    }

    FINISH:

    if(fp != NULL)
    {
        fclose(fp);
    }

    if(rsa != NULL)
    {
        RSA_free(rsa);
    }

    return iReturn;
}

bool CRsaProxy::GetPublicKey(char** strKey)
{
    //the caller must free the parameter pointer
    *strKey = new char[m_iPublicKeyLength + 1];

    memset(*strKey, 0, m_iPublicKeyLength + 1);
    memcpy(*strKey, m_strPublicKey, m_iPublicKeyLength);

    m_ErrorType = RSA_NO_ERROR;
    return true;
}

bool CRsaProxy::GetPrivateKey(char** strKey)
{
    //the caller must free the parameter pointer
    *strKey = new char[m_iPrivateKeyLength + 1];

    memset(*strKey, 0, m_iPrivateKeyLength + 1);
    memcpy(*strKey, m_strPrivateKey, m_iPrivateKeyLength);

    m_ErrorType = RSA_NO_ERROR;
    return true;
}

bool CRsaProxy::Encrypt(_RSA_KEY_TYPE enKeyType, unsigned char* strPlainText, unsigned char** strCypheredText, unsigned int* iCypheredTextLength)
{
    m_ErrorType = RSA_NO_ERROR;
    int iReturn = true;

    RSA* rsa = NULL;
    if(enKeyType == PUBLIC_KEY)
    {
        rsa = CreateRSA(m_strPublicKey, PUBLIC_KEY);

        if(rsa == NULL)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iCypheredTextLength = RSA_size(rsa);
        *strCypheredText = new unsigned char[*iCypheredTextLength];

        int iPlainTextLength = strlen((char*)strPlainText);
        int encrypted_length = RSA_public_encrypt(iPlainTextLength, strPlainText, *strCypheredText, rsa, RSA_PKCS1_PADDING);

        if(encrypted_length == -1)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iCypheredTextLength = encrypted_length;
    }
    else if(enKeyType == PRIVATE_KEY)
    {
        rsa = CreateRSA(m_strPrivateKey, PRIVATE_KEY);

        if(rsa == NULL)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iCypheredTextLength = RSA_size(rsa);
        *strCypheredText = new unsigned char[*iCypheredTextLength];

        int iPlainTextLength = strlen((char*)strPlainText);
        int encrypted_length = RSA_private_encrypt(iPlainTextLength, strPlainText, *strCypheredText, rsa, RSA_PKCS1_PADDING);

        if(encrypted_length == -1)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iCypheredTextLength = encrypted_length;
    }
    else
    {
        //unknown key type
        m_ErrorType = RSA_KEY_TYPE_NOT_DEFINED;
        iReturn = false;
        goto FINISH;
    }

    FINISH:

    if(rsa != NULL)
    {
        RSA_free(rsa);
    }

    return iReturn;
}

bool CRsaProxy::Decrypt(_RSA_KEY_TYPE enKeyType, unsigned char* strCypheredText, unsigned int iCypheredTextLength, unsigned char** strPlainText, unsigned int* iPlainTextLength)
{
    m_ErrorType = RSA_NO_ERROR;
    int iReturn = true;

    RSA* rsa = NULL;
    if(enKeyType == PUBLIC_KEY)
    {
        rsa = CreateRSA(m_strPublicKey, PUBLIC_KEY);

        if(rsa == NULL)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iPlainTextLength = RSA_size(rsa);
        *strPlainText = new unsigned char[*iPlainTextLength];

        int decrypted_length = RSA_public_decrypt(iCypheredTextLength, strCypheredText, *strPlainText, rsa, RSA_PKCS1_PADDING);

        if(decrypted_length == -1)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iPlainTextLength = decrypted_length;
    }
    else if(enKeyType == PRIVATE_KEY)
    {
        rsa = CreateRSA(m_strPrivateKey, PRIVATE_KEY);

        if(rsa == NULL)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iPlainTextLength = RSA_size(rsa);
        *strPlainText = new unsigned char[*iPlainTextLength];

        int decrypted_length = RSA_private_decrypt(iCypheredTextLength, strCypheredText, *strPlainText, rsa, RSA_PKCS1_PADDING);

        if(decrypted_length == -1)
        {
            m_ErrorType = RSA_ENCRYPTION_ERROR;
            iReturn = false;
            goto FINISH;
        }

        *iPlainTextLength = decrypted_length;
    }
    else
    {
        //unknown key type
        m_ErrorType = RSA_KEY_TYPE_NOT_DEFINED;
        iReturn = false;
        goto FINISH;
    }

    FINISH:

    if(rsa != NULL)
    {
        RSA_free(rsa);
    }

    return iReturn;
}

_RSA_ERROR_TYPE CRsaProxy::GetLastError()
{
    return m_ErrorType;
}

int CRsaProxy::GetPrivateKeyLength()
{
    return m_iPrivateKeyLength;
}

int CRsaProxy::GetPublicKeyLength()
{
    return m_iPublicKeyLength;
}

//******************Private Methods******************//
bool CRsaProxy::SetupKey(RSA* rsa, _RSA_KEY_TYPE enKeyType)
{
    if( rsa == NULL || !IS_VALID_KEY_TYPE(enKeyType) )
    {
        m_ErrorType = RSA_INVALID_PARAMETER;
        return false;
    }

    // To get the C-string PEM form:
    if(enKeyType == PUBLIC_KEY)
    {
        BIO* pub = BIO_new(BIO_s_mem());
        PEM_write_bio_RSA_PUBKEY(pub, rsa);
        m_iPublicKeyLength = BIO_pending(pub);

        if(m_strPublicKey != NULL)
        {
            delete[] m_strPublicKey;
        }

        m_strPublicKey  = new char[m_iPublicKeyLength + 1];
        memset(m_strPublicKey, 0, m_iPublicKeyLength + 1);

        BIO_read(pub, m_strPublicKey, m_iPublicKeyLength);

        BIO_free_all(pub);

        return true;
    }
    else if(enKeyType == PRIVATE_KEY)
    {
        BIO* pri = BIO_new(BIO_s_mem());

        PEM_write_bio_RSAPrivateKey(pri, rsa, NULL, NULL, 0, NULL, NULL);

        m_iPrivateKeyLength = BIO_pending(pri);

        if(m_strPrivateKey != NULL)
        {
            delete[] m_strPrivateKey;
        }

        m_strPrivateKey = new char[m_iPrivateKeyLength + 1];
        memset(m_strPrivateKey, 0, m_iPrivateKeyLength + 1);

        BIO_read(pri, m_strPrivateKey, m_iPrivateKeyLength);

        BIO_free_all(pri);

        return true;
    }
    else
    {
        //unknown type
        m_ErrorType = RSA_KEY_TYPE_NOT_DEFINED;
        return false;
    }

    return true;
}

void CRsaProxy::ReleaseMemory()
{
    if(m_strPrivateKey != NULL)
        delete[] m_strPrivateKey;

    if(m_strPublicKey != NULL)
        delete[] m_strPublicKey;
}

RSA* CRsaProxy::CreateRSA(char* strKey, _RSA_KEY_TYPE enKeyType)
{
    if(!IS_VALID_KEY_TYPE(enKeyType))
    {
        m_ErrorType = RSA_INVALID_PARAMETER;
        return NULL;
    }

    RSA* rsa= NULL;
    BIO* keybio;

    keybio = BIO_new_mem_buf(strKey, -1);
    if(keybio == NULL)
    {
        m_ErrorType = RSA_KEY_GENERATION_ERROR;
        return NULL;
    }

    if(enKeyType == PUBLIC_KEY)
    {
        rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
    }
    else
    {
        rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
    }

    delete keybio;
    return rsa;
}
