/*
 * CParseCmd.cpp
 *
 *  Created on: 31/05/2014
 *      Author: Rodrigo
 */

#include "CParseCmd.h"

#include <stdlib.h>
//#include <ctype.h>

CParseCmd::CParseCmd()
{
	// TODO Auto-generated constructor stub

}

CParseCmd::~CParseCmd()
{
	// TODO Auto-generated destructor stub
}

std::string CParseCmd::GetParameter(ParameterID idParam)
{
	std::string strParameter("");

	std::map<ParameterID, std::string>::iterator it;
	it = m_mapParameters.find(idParam);

	if( it != m_mapParameters.end() )
		strParameter = it->second;

	return strParameter;
}

bool CParseCmd::ParseCmd(int argc, char *argv[])
{
	if( argc == 1)
	{
		std::cout
		<< "Usage: SocketClientLinux -f <iniFile> [-i <ServerIP> -p <PortNumber>]"
		<< " -t <Sampling Time>"
		<< " -sec [on|off]"
		<< std::endl;
		return false;
	}

	for(int i=1; i < argc; ++i)
	{
		std::string sParam(argv[i]);

		// "-f"
		if( sParam.compare(INI_FILE_ATTRIB) == 0 )
		{
			if( i < argc-1 )
			{
				std::string sArg(argv[++i]);
				m_mapParameters.insert( std::make_pair(ID_INI_FILE, sArg) );
			}
			else
			{
				std::cout << "Missing argument for " << INI_FILE_ATTRIB << " parameter" << std::endl;
				return false;
			}
		}

		//"-i"
		if( sParam.compare(INI_IP_ATTRIB) == 0 )
		{
			if( i < argc-1 )
			{
				std::string sArg(argv[++i]);
				m_mapParameters.insert( std::make_pair(ID_IP_ADDRESS, sArg) );
			}
			else
			{
				std::cout << "Missing argument for " << INI_IP_ATTRIB << " parameter" << std::endl;
				return false;
			}
		}

		//"-p"
		if( sParam.compare(INI_PORT_ATTRIB) == 0 )
		{
			if( i < argc-1 )
			{
				std::string sArg(argv[++i]);
				m_mapParameters.insert( std::make_pair(ID_PORT_NUMBER, sArg) );
			}
			else
			{
				std::cout << "Missing argument for " << INI_PORT_ATTRIB << " parameter" << std::endl;
				return false;
			}
		}

		//"-t"
		if( sParam.compare(INI_SAMPLE_TIME) == 0 )
		{
			if( i < argc-1 )
			{
				std::string sArg(argv[++i]);
				m_mapParameters.insert( std::make_pair(ID_TIME_SAMPLE, sArg) );
			}
			else
			{
				std::cout << "Missing argument for " << INI_SAMPLE_TIME << " parameter" << std::endl;
				return false;
			}
		}

		//"-SEC"
		if( sParam.compare(INI_SECURITY_MODE) == 0 )
		{
			if( i < argc-1 )
			{
				std::string sArg(argv[++i]);
				m_mapParameters.insert( std::make_pair(ID_SECURITY_MODE, sArg) );
			}
			else
			{
				std::cout << "Missing argument for " << INI_SECURITY_MODE << " parameter" << std::endl;
				return false;
			}
		}
	}

	return true;
}


