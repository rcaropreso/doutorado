#ifndef FOURIER_H_
#define FOURIER_H_

#include <iostream>
#include "complexo.h"
#include <math.h>
 
using namespace std;
 
 class Fourier 
 {
 	private:
		int Npontos;
		Complexo *vetorComplexo;
		int Log2N;
		Complexo Wr[20];
	public:
		Fourier(int N, double *vetor);
		~Fourier();
		void fft(double *saida);
		void reord();
		void troca(Complexo *c1, Complexo *c2);
		void factores();
		void dft2();
		void borbol();
		void imprimir();
 };

#endif /* FOURIER_H_ */
