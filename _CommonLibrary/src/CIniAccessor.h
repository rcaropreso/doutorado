/*
 * CIniAccessor.h
 *
 *  Created on: 30/05/2014
 *      Author: Rodrigo
 */

#ifndef CINIACCESSOR_H_
#define CINIACCESSOR_H_

#include <map>
#include <string>
#include <iostream>
//#include <vector>
#include <fstream>

typedef struct
{
	size_t iSectionBeginPos;
	size_t iSectionEndPos;
	size_t iEqualSignPos;
	size_t iCommentStartPos;
} _LINEINFO, *_PLINEINFO;

enum LineType
{
	LT_ERROR = -1,
	LT_SECTION,
	LT_KEY
};

class CIniAccessor
{
public:
	CIniAccessor();
	virtual ~CIniAccessor();

	bool ReadIniFile(std::string sFileName);

	std::string GetKeyValue(std::string sSection, std::string sKey);

private:
	//The ini is inserted into a std::map withthe pair
	//Key     -> Section
	//Element -> a reference to a vector
	//Each vector contains a std:pair element which represents key=value pair
	//for a given section

	typedef std::pair<std::string, std::string> keyPair;
	typedef std::map<std::string, std::string> keysMap;
	std::map<std::string, keysMap*> m_mapIni;

	bool ParseIniFile(std::ifstream &fsIniFile);
	LineType ValidateLine(std::string line, size_t iLineIndex, const _PLINEINFO pLineInfo);
	std::string trim(std::string const& str);

	void DebugPrint();
	void ClearIni();
};

#endif /* CINIACCESSOR_H_ */

