//============================================================================
// Name        : SocketServer.cpp
// Author      : Rodrigo Caropreso
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>
#include "CServer.h"

using namespace std;

//#define __LINUX
/*
 * SocketServerWin <server-ip> <port-number>
 */

#define _PRINT_MESSAGES_ON

int main(int argc, char *argv[])
{
#ifdef __WINDOWS
	system( "cls" );
#endif

#ifdef __LINUX
	system( "clear" );
#endif

	if( argc < 3 )
	{
		cout
		    << "Usage: SocketServer <ServerIP> <PortNumber>"
		    << endl;
		return 0;
	}

	string sServerIP( argv[1] );
	int iPortNumber = atoi(argv[2]);

	CServer* oServer = new CServer(sServerIP, iPortNumber);

	oServer->StartListen();
	int counter=0;
	while(counter < 3)
	{
		SOCKET sock = oServer->GetHandle();
		sock = oServer->WaitForConnection(sock);
		counter++;
	}
	oServer->StopListen();

	delete oServer;

	return 0;
}
