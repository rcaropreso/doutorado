#include "CHashUnitTest.h"

CHashUnitTest::CHashUnitTest()
{
    m_LogType = OUT_SCREEN;
    m_fpLogFile = NULL;
}

CHashUnitTest::CHashUnitTest(string strFileName)
{
    m_LogType = OUT_FILE;
    m_fpLogFile = fopen(strFileName.c_str(), "w");

    if(m_fpLogFile == NULL)
    {
        cout << "Error opening log file!" << endl;
    }
}

CHashUnitTest::~CHashUnitTest()
{
    if(m_fpLogFile != NULL)
    {
        fclose(m_fpLogFile);
    }
}

void CHashUnitTest::LogOutput(char* strMessage, int iMessageLength)
{
    if(m_LogType == OUT_SCREEN)
    {
        cout << strMessage << endl;
    }
    else if(m_LogType == OUT_FILE)
    {
        fwrite(strMessage, 1, iMessageLength, m_fpLogFile);
    }
}

void CHashUnitTest::RunHashTest()
{
    RunHashTestMD5();

    RunHashTestSHA256();
}

bool CHashUnitTest::RunHashTestMD5()
{
    stringstream ssMsg;

    ssMsg << "Starting Hash tests..." << endl;

    //Hash test -  BEGIN
    CHashProxy* oHash = new CHashProxy();

    unsigned char strPlainText[8] = "Rodrigo";
    unsigned int iPlainTextLength = strlen((char*)strPlainText);

    //*****MD5 Hash*****
    ssMsg << "  MD5 Hash for 'Rodrigo'..." << endl;

    int iSizeForHash = oHash->GetAllocationSizeForMD5();
    unsigned char* strHashText = new unsigned char[iSizeForHash];
    memset(strHashText, 0, iSizeForHash);

    if(!oHash->GenerateMD5(strPlainText, iPlainTextLength))
    {
        ssMsg << "Error on MD5 GenerateMD5() operation" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    oHash->GetDigestMD5(strHashText);

    //Send to output
    string sText;
    char strAux[3];
    for(int i=0; i < iSizeForHash; i++)
    {
        sprintf(strAux, "%02x", strHashText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nHash for MD5 is " << iSizeForHash << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strHashText;
    delete oHash;

    return true;
}

bool CHashUnitTest::RunHashTestSHA256()
{
    stringstream ssMsg;

    //Hash test -  BEGIN
    CHashProxy* oHash = new CHashProxy();

    unsigned char strPlainText[8] = "Rodrigo";
    unsigned int iPlainTextLength = strlen((char*)strPlainText);

    //*****Sha256 Hash*****
    ssMsg << "  SHA256 Hash for 'Rodrigo'..." << endl;

    int iSizeForHash = oHash->GetAllocationSizeForSHA256();
    unsigned char* strHashText = new unsigned char[iSizeForHash];
    memset(strHashText, 0, iSizeForHash);

    if( !oHash->GenerateSHA256(strPlainText, iPlainTextLength) )
    {
        ssMsg << "Error on SHA256 GenerateSHA256() operation" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    if( !oHash->GetDigestSHA256(strHashText) )
    {
        ssMsg << "Error on SHA256 GetDigestSHA256() operation" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Send to output
    const int DIGEST_SIZE_BYTES = 256/8;

    string sText;
    char strAux[3];
    for(int i=0; i < DIGEST_SIZE_BYTES; i++)
    {
        sprintf(strAux, "%02x", strHashText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nHash for SHA256 is " << DIGEST_SIZE_BYTES << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strHashText;
    delete oHash;

    return true;
}
