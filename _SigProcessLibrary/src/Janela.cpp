#include "Janela.h"

Janela::Janela(int Npontos){
	this->Npontos = Npontos;
	this->absFourier = new double[Npontos/2];
	this->sinal = new double[Npontos];
}

Janela::Janela(int Npontos,double *sinal){
	this->Npontos = Npontos;
	this->absFourier = new double[Npontos/2];
	this->sinal = sinal;
}

void Janela::inserir(int indice, double ponto){
	if(indice < this->Npontos)
		sinal[indice] = ponto;
}

void Janela::calculaRMS(){
	double somatorio = 0;
	for(int i = 0; i < this->Npontos; i++){
		somatorio = somatorio + (sinal[i] * sinal[i]);
	}
	this->RMS = sqrt(somatorio/this->Npontos);
	//cout << "RMS: " << RMS << "\n";
}

void Janela::calculaMedia(){
	double somatorio = 0;
	for(int i = 0; i < this->Npontos; i++){
		somatorio = somatorio + sinal[i];
	}
	this->media = (somatorio/this->Npontos);
	//cout << "DHT: " << DHT << "\n";
}

void Janela::calculaMediaHarmonica(){
	double somatorio = 0;
	for(int i = 0; i < this->Npontos; i++){
		somatorio = somatorio + (1/sinal[i]);
	}
	this->mediaHarmonica = (this->Npontos/somatorio);
}

void Janela::calculaDesvioPadrao(){
	double somatorio = 0;
	for(int i = 0; i < this->Npontos; i++){
		somatorio = somatorio + pow((sinal[i] - media),2);
	}
	this->desvioPadrao = sqrt((somatorio/(this->Npontos-1)));
	
}

void Janela::calculaDesvioMedio(){
	double somatorio = 0;	
	for(int i = 0; i < this->Npontos; i++){
		somatorio = somatorio + fabs(sinal[i] - this->media);
	}
	this->desvioMedio = (somatorio/this->Npontos);
}


void Janela::calculaErroPadrao(){
	//double somatorio = 0;
	calculaDesvioPadrao();		
	this->erroPadrao = this->desvioPadrao/sqrt(this->Npontos);
}

void Janela::calculaValorPico(){
	this->pico = sinal[0];
	for(int i = 0; i < this->Npontos; i++){	
		if(fabs(sinal[i]) > this->pico)
			this->pico = sinal[i];
	}
}

void Janela::calculaFatorCrista(){
	this->fatorCrista = this->pico/this->RMS;
}

void Janela::calculaFatorForma(){
	this->fatorForma = this->media/this->RMS;
}

void Janela::calculaKurtosis(){
	double somatorio1 = 0;
	double somatorio2 = 0;
	for(int i = 0; i < this->Npontos; i++){
		somatorio1 = somatorio1 + pow((sinal[i] - this->media),4);
		somatorio2 = somatorio2 + pow((sinal[i] - this->media),2);
	}
	this->kurtosis = (somatorio1/this->Npontos)/pow((somatorio2/this->Npontos),2);
	
}

void Janela::calculaMaxMin(){
	double maximo = sinal[0], minimo = sinal[0];
	for(int i = 0; i < this->Npontos; i++){	
		if((sinal[i] > maximo))
			maximo = this->sinal[i];
		if((sinal[i] < minimo))
			minimo = this->sinal[i];
	}
	this->maxMin = maximo - minimo;
}

void Janela::calculaEntropia(){
	//double somatorio = 0;
	this->entropia = 0;
	for(int i = 0; i < this->Npontos; i++){
		this->entropia = this->entropia + log(pow(sinal[i],2));
	}
}

void Janela::calculaEntropiaShannon(){
	//double somatorio = 0;
	this->entropiaShannon = 0;
	for(int i = 0; i < Npontos; i++){
		this->entropiaShannon = this->entropiaShannon + (pow(sinal[i],2) * log(pow(sinal[i],2)));
	}
	this->entropiaShannon = this->entropiaShannon* - 1;
}




void Janela::calculaDHT(){
	//double absFourier[Npontos/2];
	double somatorio = 0;
	Fourier *fourier = new Fourier(Npontos, sinal);
	
	fourier->fft(this->absFourier);
	fourier->~Fourier();
	for(int i = 2; i < Npontos/2; i++){ //i=0 ou 1
		somatorio = somatorio + (this->absFourier[i] * this->absFourier[i]);
		
	}
	DHT = sqrt(somatorio)/this->absFourier[1];
	//cout << "DHT: " << DHT << "\n";
}

void Janela::detecao(double DHTreferencia, double limiarDHT, double RMSreferencia,  double limiarRMS){
	int flagDHT = 0, flagRMS = 0; 
	double aux = DHT-DHTreferencia;
	
	double distDHT = sqrt(aux*aux);
	if(distDHT > limiarDHT)
		flagDHT = 1;
	
	aux = RMS-RMSreferencia;
	double distRMS = sqrt(aux*aux);
	if(distRMS > limiarRMS)
		flagRMS = 1;
	
	this->flagDetecao = flagRMS + flagDHT;
	//cout << "Detecao: " << flagDetecao << "\n";
}

void Janela::setSinal(double *sinal){
	this->sinal = sinal;
}

int Janela::getDetecao(){
	return flagDetecao;	
}

void Janela::relativoHarmonica(){
	relativoFourier = new double[Npontos/2];
	for(int i = 0; i < Npontos/2; i++){ //i=0 ou 1
		relativoFourier[i] = absFourier[i]/absFourier[1];
		
	}
}


double* Janela::extrairCaracteristicas(){

	double *caracteristicas = new double[13];
	
	calculaMedia();
	calculaRMS();
	calculaValorPico();
	calculaDesvioMedio();
	calculaDesvioPadrao();
	calculaEntropia();
	calculaEntropiaShannon();
	calculaFatorCrista();
	calculaFatorForma();
	calculaKurtosis();
	calculaMaxMin();
	calculaMediaHarmonica();
	calculaDHT();
	
	caracteristicas[0] = this->media;
	caracteristicas[1] = this->RMS;
	caracteristicas[2] = this->pico;
	caracteristicas[3] = this->desvioMedio;
	caracteristicas[4] = this->desvioPadrao;
	caracteristicas[5] = this->entropia;
	caracteristicas[6] = this->entropiaShannon;
	caracteristicas[7] = this->fatorCrista;
	caracteristicas[8] = this->fatorForma;
	caracteristicas[9] = this->kurtosis;
	caracteristicas[10] = this->maxMin;
	caracteristicas[11] = this->mediaHarmonica;
	caracteristicas[12] = this->DHT;
	//relativoHarmonica();
	
	return caracteristicas;
}
