#ifndef JANELA_H_
#define JANELA_H_

#include <iostream>
#include <math.h> 
#include "Fourier.h"
 
using namespace std;
 
 class Janela 
 {
 	private:
 		int Npontos;
 		double *relativoFourier;
 		double *sinal;
 		double *absFourier;
 		double RMS;
 		double DHT;
 		double media;
 		double mediaHarmonica;
 		double entropia;
 		double entropiaShannon;
 		double desvioMedio;
 		double desvioPadrao;
 		double erroPadrao;
 		double pico;
 		double fatorCrista;
 		double fatorForma;
 		double kurtosis;
 		double maxMin;
 		int flagDetecao;
	public:
		Janela(int Npontos,double *sinal);
		Janela(int Npontos);
		void inserir(int indice, double ponto);
		void calculaRMS();
		void calculaDHT();
		void detecao(double DHTreferencia, double limiarDHT, double RMSreferencia,  double limiarRMS);
		int getDetecao();
		void setSinal(double *sinal);
		void calculaMedia();
		void calculaMediaHarmonica();
		void calculaDesvioMedio();
		void calculaEntropia();
		void calculaEntropiaShannon();
		void calculaDesvioPadrao();
		void calculaErroPadrao();
		void calculaValorPico();
		void calculaFatorCrista();
		void calculaFatorForma();
		void calculaMaxMin();
		void calculaKurtosis();
		double* extrairCaracteristicas();
		void relativoHarmonica();
 };
 
#endif /* JANELA_H_ */
