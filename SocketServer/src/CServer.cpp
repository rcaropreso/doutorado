/*
 * CServer.cpp
 *
 *  Created on: 01/05/2014
 *      Author: Rodrigo
 */

#include "CServer.h"
#include "CSocketManager.h"

#ifdef __LINUX
	#include <string.h>
	#include <sstream>
	#include <errno.h>
	#include <arpa\inet.h>
#endif

CServer::CServer(string sIP, long lPort) :
		m_sIPAddress(sIP), m_lPortNumber(lPort)
{
	m_uSocket = 0;
	m_bSocketOpened = false;
}

CServer::~CServer()
{
	StopListen();
}

void CServer::StopListen()
{
	//WinSock Cleanup
#ifdef __WINDOWS
	if(IsOpen())
	{
		shutdown(m_uSocket, 0x02/*SD_BOTH*/);
		closesocket (m_uSocket);
	}

	WSACleanup();
#endif

#ifdef __LINUX
	if(IsOpen())
	{
		close(m_uSocket);
	}
#endif

	m_bSocketOpened = false;
}

bool CServer::StartListenForWindows()
{
#ifdef __WINDOWS
	WSADATA w;
    int iResult = WSAStartup (0x0202, &w);   // Fill in WSA info
	if (iResult != 0)//For some reason we couldn't start Winsock
	{
		cout
		    << "This version is not supported!"
		    << WSAGetLastError()
		    << endl;
		return false;
	}
	else
	{
		cout
		    << "Winsock version OK!"
		    << endl;
	}

    if (w.wVersion != 0x0202) //Wrong Winsock version?
    {
    	cout
    	    << "Winsock version is NOT 0x0202"
    	    << endl;
        WSACleanup ();
        return false;
    }

    //Setup SOCKADDR_IN
    SOCKADDR_IN addr; // The address structure for a TCP socket

    addr.sin_family = AF_INET;      // Address family
    addr.sin_port = htons (m_lPortNumber);   // Assign port to this socket

    //Accept a connection from any IP using INADDR_ANY
    //You could pass inet_addr("0.0.0.0") instead to accomplish the
    //same thing. If you want only to watch for a connection from a
    //specific IP, specify that //instead.
    if( m_sIPAddress.empty() )
    	addr.sin_addr.s_addr = htonl (INADDR_ANY);
    else
    	addr.sin_addr.s_addr = inet_addr( m_sIPAddress.c_str() );

    //Create Socket
    m_uSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); // Create socket

    if (m_uSocket == INVALID_SOCKET)
    {
		cout
		    << "Socket creation FAILED"
		    << endl;
        return false; //Don't continue if we couldn't create a //socket!!
    }
    else
    	m_bSocketOpened = true;

    if (bind(m_uSocket, (LPSOCKADDR)&addr, sizeof(addr)) == SOCKET_ERROR)
    {
       //We couldn't bind (this will happen if you try to bind to the same
       //socket more than once)
		cout
		    << "Socket BINDING FAILED"
		    << endl;
        return false;
    }

    //Now we can start listening (allowing as many connections as possible to
    //be made at the same time using SOMAXCONN). You could specify any
    //integer value equal to or lesser than SOMAXCONN instead for custom
    //purposes). The function will not //return until a connection request is
    //made
    if( listen(m_uSocket, SOMAXCONN) == SOCKET_ERROR )
    {
    	StopListen();
    	return false;
    }

    cout
        << "Server <Windows> listening on adress: "
        << m_sIPAddress
        << ":"
        << m_lPortNumber
        << endl;

    while( IsOpen() )
    {
    	cout << "Waiting new connection..." << endl;

    	SOCKET sock = WaitForConnection( this->m_uSocket );

    	cout
    	    << "Connection arrived!"
    	    << endl;

    	cout
    	    << "\nSERVER socket = "
    	    << this->m_uSocket
    	    << " CONNECTION socket: "
    	    << sock
    	    << endl;

    	if( sock != INVALID_SOCKET )
    		HandleAcceptedConnection(sock);
    }

    //Don't forget to clean up with CloseConnection()!
    return true;
#endif

	return false; //if we got here, it is NOT WINDOWS!
}

bool CServer::StartListenForLinux()
{
#ifdef __LINUX
	m_uSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (m_uSocket < 0)
	{
		cout
		    << "Socket creation FAILED"
		    << endl;
		return false;
	}
	else
	{
		cout
		    << "Socket creation SUCCESSFULL"
		    << endl;
    	m_bSocketOpened = true;
	}

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));

	 if(inet_pton(AF_INET, m_sIPAddress.c_str(), &addr.sin_addr)<=0)
	 {
		cout
		    << "ERROR on address conversion."
		    << endl;
		close( m_uSocket );
		return false;
	 }

	//Preenche a estrutura
	addr.sin_family = AF_INET;
	addr.sin_port = htons(m_lPortNumber);

	if( bind(m_uSocket, (struct sockaddr*)&addr, sizeof(addr)) < 0 )
	{
		cout
		    << "ERROR on BINDING."
		    << endl;
		close( m_uSocket );
		return false;
	}

    //Now we can start listening (allowing as many connections as possible to
    //be made at the same time using SOMAXCONN). You could specify any
    //integer value equal to or lesser than SOMAXCONN instead for custom
    //purposes). The function will not //return until a connection request is
    //made
    if( listen(m_uSocket, SOMAXCONN) < 0 )
    {
		cout
		    << "Listen ERROR - "
		    << errno
		    << endl;
		close( m_uSocket );
		StopListen();
		return false;
    }

    cout
        << "Server <Linux> listening on adress: "
        << m_sIPAddress
        << ":"
        << m_lPortNumber
        << endl;

    while( IsOpen() )
    {
    	cout
    	    << "Waiting new connection..."
    	    << endl;

    	SOCKET sock = WaitForConnection( this->m_uSocket );

    	cout
    	    << "Connection arrived!"
    	    << endl;

    	cout
    	    << "\nSERVER socket = "
    	    << this->m_uSocket
    	    << " CONNECTION socket: "
    	    << sock
    	    << endl;

    	if( sock > 0 )
    		HandleAcceptedConnection(sock);
    }

    //Don't forget to clean up with CloseConnection()!
    return true;

#endif
	return false;  //if we got here, it is NOT LINUX!
}

bool CServer::StartListen()
{
	if( IsOpen() )
		return false;

#ifdef __WINDOWS
	return StartListenForWindows();
#endif

#ifdef __LINUX
	return StartListenForLinux();
#endif

}

/*
 * This method creates a thread to handle the result of an
 * accepted connection
 */
void CServer::HandleAcceptedConnection( SOCKET sock )
{
    pthread_t thread1;
    int i1;

    stringstream ss;//create a string stream
    ss << "SocketThread " << sock;//add number to the stream

    CSocketManager* oManager = new CSocketManager(sock, 0);
    oManager->SetMessage( ss.str() );

	cout
	    << "Opening Connection thread for socket: "
	    << sock
	    << endl;

    i1 = pthread_create( &thread1, NULL, CServer::threadConnection, (void*) oManager );

    if( i1 )
    {
    	cout
    	    << "ERROR! Thread could not be created for socket "
    	    << sock
    	    << endl;
    }
}

/*
 * Thread which handles a connection
 */
void* CServer::threadConnection(void *obj)
{
    //All we do here is call the do_work() function
	CSocketManager* oManager = reinterpret_cast<CSocketManager*>(obj);

	cout
	    << "    *Connection thread for socket: "
	    << oManager->GetSocket()
	    << " : BEGIN" << endl;

	if( oManager )
	{
		oManager->OnHandleConnection();
	}

	cout
	    << "    *Connection thread for socket: "
	    << oManager->GetSocket()
	    << " : END"
	    << endl;

	pthread_exit(NULL);
	return NULL;
}

SOCKET CServer::GetHandle()
{
	return m_uSocket;
}

SOCKET CServer::WaitForConnection(SOCKET sock)
{
	// Accept an incoming connection - blocking
	// no information about remote address is returned
	return accept(sock, 0, 0);
}

