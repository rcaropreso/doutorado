#ifndef CRSAUNITTEST_H
#define CRSAUNITTEST_H

#include "CRsaproxy.h"
#include "CSecurityCommonDefs.h"

class CRsaUnitTest
{
public:
    CRsaUnitTest();
    CRsaUnitTest(string strFileName);
    ~CRsaUnitTest();

    void RunRSATest();

//private:
    bool RunRSALoadKeysTest(CRsaProxy* oRSA);
    bool RunRSAEncryptionTest(CRsaProxy* oRSA);
    bool RunRSAEncryptionTest2(CRsaProxy* oRSA);
    bool RunRSAKeysTest(CRsaProxy* oRSA);
    void LogOutput(char* strMessage, int iMessageLength);

    _LOG_OUTPUT_TYPE m_LogType;
    FILE* m_fpLogFile;
};

#endif // CRSAUNITTEST_H
