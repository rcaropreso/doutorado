/*
 * CIniAccessor.cpp
 *
 *  Created on: 30/05/2014
 *      Author: Rodrigo
 */

#include "CIniAccessor.h"

using namespace std;

CIniAccessor::CIniAccessor()
{
	// TODO Auto-generated constructor stub

}

CIniAccessor::~CIniAccessor()
{
	// TODO Auto-generated destructor stub
	ClearIni();
}

/*
 * Read the ini File an load it into map member
 */
bool CIniAccessor::ReadIniFile(std::string sFileName)
{
	bool bRet = true;

	std::ifstream fsIniFile;
	fsIniFile.open(sFileName.c_str(), std::ios::in);
	if (fsIniFile.is_open())
	{
		bRet = ParseIniFile(fsIniFile);
		fsIniFile.close();
	}

	return bRet;
}

bool CIniAccessor::ParseIniFile(std::ifstream &fsIniFile)
{
	std::string line;
	size_t uiLineIndex = 0;
	_LINEINFO stLineInfo;
	std::string sCurrentSection;

	while ( getline (fsIniFile, line) )
	{
		uiLineIndex++;
		if( line.empty() )
			continue;

		//Parsing
		LineType lt = ValidateLine(line, uiLineIndex, &stLineInfo);

		if( lt == LT_SECTION )
		{
			//1 - Get Section name
			//2 - Set current Section Name
			sCurrentSection = line.substr( stLineInfo.iSectionBeginPos+1,
					stLineInfo.iSectionEndPos-stLineInfo.iSectionBeginPos-1);

			sCurrentSection = trim(sCurrentSection);

			cout << "Section=\"" << sCurrentSection << "\"" << endl;

			//3 - Get comment
			if(stLineInfo.iCommentStartPos != string::npos )
			{
				std::string sSectionComment = line.substr( stLineInfo.iCommentStartPos+1,
						line.length()-stLineInfo.iCommentStartPos-1);

				sSectionComment = trim(sSectionComment);

				cout << "Section Comment=\"" << sSectionComment << "\"" << endl;
			}

			//4 - Add to map
			if( m_mapIni.find(sCurrentSection) != m_mapIni.end() )
			{
				cout << "SYNTAX ERROR: Repeated Section \"" << sCurrentSection << "\" at Line: " << uiLineIndex << endl;
				return false;
			}

			//Create a Keys Vector for the current section
			//keysMap *vKeys = new std::vector<keyPair*>();
			keysMap* keys = new std::map<std::string, std::string>();
			m_mapIni[sCurrentSection] = keys;
			//m_mapIni.insert( std::pair<std::string, keysVector*>(sCurrentSection, vKeys) );
		}

		if( lt == LT_KEY )
		{
			//1 - Get key name
			std::string sKeyName = line.substr( 0,
					stLineInfo.iEqualSignPos-1);

			sKeyName = trim(sKeyName);

			cout << "KeyName=\"" << sKeyName << "\"" << endl;

			//2 - Get value
			std::string sKeyValue = line.substr( stLineInfo.iEqualSignPos+1,
					stLineInfo.iCommentStartPos-stLineInfo.iEqualSignPos-1);

			sKeyValue = trim(sKeyValue);

			cout << "KeyValue=\"" << sKeyValue << "\"" << endl;

			//3 - Get comment
			if(stLineInfo.iCommentStartPos != string::npos )
			{
				std::string sSectionComment = line.substr( stLineInfo.iCommentStartPos+1,
						line.length()-stLineInfo.iCommentStartPos-1);

				sSectionComment = trim(sSectionComment);

				cout << "Key Comment=\"" << sSectionComment << "\"" << endl;
			}

			//4 - Add to map using current section name
			//keyPair *pairKeyValue = new keyPair(sKeyName, sKeyValue);
			//m_mapIni[sCurrentSection]->push_back(pairKeyValue);
			keysMap *keys = (keysMap*)m_mapIni[sCurrentSection];
			keys->insert( std::make_pair(sKeyName, sKeyValue) );//[sKeyName] = sKeyValue;
		}

		//cout << line << '\n';
	}

	DebugPrint();

	return true;
}

void CIniAccessor::DebugPrint()
{
	std::map<std::string, keysMap*>::iterator itSections = m_mapIni.begin();

	while( itSections != m_mapIni.end())
	{
		cout << "\"" << itSections->first << "\"" << endl;

		keysMap* vKeys = (keysMap*) itSections->second;
		std::map<std::string, std::string>::iterator itKeys = vKeys->begin();
		while( itKeys != vKeys->end() )
		{
			cout << "\t " << itKeys->first << "=" << itKeys->second << endl;
			itKeys++;
		}
		itSections++;
	}
}
std::string CIniAccessor::GetKeyValue(std::string sSection, std::string sKey)
{
	std::map<std::string, keysMap*>::iterator itSections;
	itSections = m_mapIni.find(sSection);

	if( sSection.empty() || itSections == m_mapIni.end() )
		return std::string("");

	std::map<std::string, std::string>::iterator itKeys;
	itKeys = ((keysMap*)itSections->second)->find(sKey);

	if( sKey.empty() || itKeys == ((keysMap*)itSections->second)->end())
		return std::string("");

	return (std::string) itKeys->second;
}

void CIniAccessor::ClearIni()
{
	std::map<std::string, keysMap*>::iterator itSections;

	for( itSections = m_mapIni.begin(); itSections != m_mapIni.end(); ++itSections)
	{
		keysMap* keys = (keysMap*) itSections->second;
		delete keys;
	}
}

LineType CIniAccessor::ValidateLine(std::string line, size_t iLineIndex, const _PLINEINFO pLineInfo)
{
	LineType lt = LT_SECTION;

	size_t iSectionBeginPos = line.find('[');
	size_t iSectionEndPos   = line.find(']');
	size_t iCommentStartPos = line.find(';');
	size_t iEqualSignPos    = line.find('=');

	pLineInfo->iSectionBeginPos = iSectionBeginPos;
	pLineInfo->iSectionEndPos   = iSectionEndPos;
	pLineInfo->iCommentStartPos = iCommentStartPos;
	pLineInfo->iEqualSignPos    = iEqualSignPos;

	//Syntax Validation - Begin
	//Repeated chars
	if (iSectionBeginPos != string::npos &&
			line.find('[', iSectionBeginPos+1) != string::npos)
	{
		cout << "SYNTAX ERROR: Repeated '[' at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	if (iSectionEndPos != string::npos &&
			line.find(']', iSectionEndPos+1) != string::npos)
	{
		cout << "SYNTAX ERROR: Repeated ']' at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	if (iEqualSignPos != string::npos &&
			line.find('=', iEqualSignPos+1) != string::npos)
	{
		cout << "SYNTAX ERROR: Repeated '=' at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	//Empty values
	if( iSectionBeginPos != string::npos && iSectionEndPos != string::npos &&
		iSectionBeginPos == iSectionEndPos )
	{
		cout << "SYNTAX ERROR: Empty Section NAME at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	if( iEqualSignPos == 0 )
	{
		cout << "SYNTAX ERROR: Empty Key NAME at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	//characters in invalid positions
	if (iSectionEndPos != string::npos && iCommentStartPos != string::npos &&
	    iCommentStartPos < iSectionEndPos)
	{
		cout << "SYNTAX ERROR: \";\" before \"]\" at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	if (iCommentStartPos != string::npos && iEqualSignPos != string::npos &&
	    iCommentStartPos < iEqualSignPos)
	{
		cout << "SYNTAX ERROR: \";\" before \"=\" at Line: " << iLineIndex << endl;
		return LT_ERROR;
	}

	//Line has '['
	if (iSectionBeginPos != string::npos )
	{
		if (iEqualSignPos != string::npos )
		{
			// '[' and '=' on same line?
			cout << "SYNTAX ERROR: 2 tokens on same line, at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}

		if (iSectionEndPos == string::npos )
		{
			//Missing ']'
			cout << "SYNTAX ERROR: Missing \"]\" at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}

		if (iSectionEndPos != string::npos &&
		    iSectionEndPos < iSectionBeginPos)
		{
			//Exchanged ']' x '['
			cout << "SYNTAX ERROR: \"[\" and \"]\" are on exchanged positions at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}
	}

	//Line does not have '['
	if (iSectionBeginPos == string::npos)
	{
		if (iSectionEndPos != string::npos && iEqualSignPos != string::npos)
		{
			// ']' and '=' on same line?
			cout << "SYNTAX ERROR: 2 tokens on same line, at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}

		if (iSectionEndPos != string::npos && iEqualSignPos == string::npos)
		{
			//Missing '['
			cout << "SYNTAX ERROR: Missing \"[\" at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}

		if (iSectionEndPos == string::npos && iEqualSignPos == string::npos)
		{
			//Unknown line Missing '[' or '='
			cout << "SYNTAX ERROR: Missing \"[\" or \"=\" at Line: " << iLineIndex << endl;
			return LT_ERROR;
		}
	}

	//Set line type
	if( iSectionBeginPos != string::npos )
		lt = LT_SECTION;

	if( iEqualSignPos != string::npos )
		lt = LT_KEY;

	return lt;
}


std::string CIniAccessor::trim(std::string const& str)
{
	std::string strRet;
    std::size_t first = str.find_first_not_of(' ');
    std::size_t last  = str.find_last_not_of(' ');
    strRet = str.substr(first, last-first+1);

    first = strRet.find_first_not_of('\t');
    last  = strRet.find_last_not_of('\t');
    strRet = strRet.substr(first, last-first+1);

    return strRet;
}
