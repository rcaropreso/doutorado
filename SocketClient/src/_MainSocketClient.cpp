//============================================================================
// Name        : SocketClient.cpp
// Author      : Rodrigo de T. Caropreso
// Version     :
// Copyright   : Your copyright notice
// Description : Socket Client App. for Windows in C++, Ansi-style
//============================================================================

#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "CHashProxy.h"
#include "CHashUnitTest.h"
#include "CAesUnitTest.h"
#include "CRsaunittest.h"

//#include <unistd.h>

#include "CClient.h"
#include "CSocketData.h"

#include "CParseCmd.h"
#include "CIniAccessor.h"

#include "Janela.h"
#include "Complexo.h"

#include "CXmlParser.h"

#define PTS 64

#define _KB 1024
#define _MB (1024*_KB)
#define _GB (1024*_MB)

double sinal[PTS] = {-69.8529770000000,-79.9127010000000,-89.2028230000000,-97.6338710000000,-105.124650000000,-111.603030000000,-117.006600000000,-121.283340000000,-124.392050000000,-126.302800000000,-126.997190000000,-126.468520000000,-124.721890000000,-121.774120000000,-117.653600000000,-112.400010000000,-106.063950000000,-98.7064350000000,-90.3983220000000,-81.2196230000000,-71.2587360000000,-60.6115880000000,-49.3807170000000,-37.6742830000000,-25.6050250000000,-13.2891760000000,-0.845345770000000,11.6066260000000,23.9468190000000,36.0563920000000,47.8187220000000,59.1205310000000,69.8529770000000,79.9127010000000,89.2028230000000,97.6338710000000,105.124650000000,111.603030000000,117.006600000000,121.283340000000,124.392050000000,126.302800000000,126.997190000000,126.468520000000,124.721890000000,121.774120000000,117.653600000000,112.400010000000,106.063950000000,98.7064350000000,90.3983220000000,81.2196230000000,71.2587360000000,60.6115880000000,49.3807170000000,37.6742830000000,25.6050250000000,13.2891760000000,0.845345770000000,-11.6066260000000,-23.9468190000000,-36.0563920000000,-47.8187220000000,-13.6103140000000};

double RMSReferencia = 0.708;
double limiarRMS = 0.03;
double DHTReferencia = 0.0278;
double limiarDHT = 0.06;

using namespace std;

void* threadSignalSampling(void *obj);

//#define __LINUX
//#define __WINDOWS

/*
 * References:
 * http://www.codeproject.com/Articles/13071/Programming-Windows-TCP-Sockets-in-C-for-the-Begin
 * http://blog.pusheax.com/2013/07/windows-api-winsock-create-your-first.html
 * https://groups.google.com/forum/#!categories/beagleboard/pru
 * https://groups.google.com/forum/#!msg/beagleboard/0a4tszlq2y0/FU9yAM17P_8J
 */

#define _PRINT_MESSAGES_ON
#define _USE_PRU_OFF

int main(int argc, char *argv[])
{
#ifdef __WINDOWS
	system( "cls" );
#endif

#ifdef __LINUX
	system( "clear" );
#endif

	//	//Teste de acesso a biblioteca
	//	Complexo* n1 = new Complexo();
	//	n1->setReal(3);
	//	n1->setImag(4);
	//
	//	cout << "The module is: " << n1->abs() << endl;
	//
	//	delete n1;
	//
	//	return 0;

	// Teste de acesso ao XML Parser - BEGIN
	//	// this open and parse the XML file:
	//	  XMLNode xMainNode=XMLNode::openFileHelper("PMMLModel.xml","PMML");
	//
	//	  // this prints "<Condor>":
	//	  XMLNode xNode=xMainNode.getChildNode("Header");
	//	  printf("Application Name is: '%s'\n", xNode.getChildNode("Application").getAttribute("name"));
	//
	//	  // this prints "Hello world!":
	//	  printf("Text inside Header tag is :'%s'\n", xNode.getText());
	//
	//	  // this gets the number of "NumericPredictor" tags:
	//	  xNode=xMainNode.getChildNode("RegressionModel").getChildNode("RegressionTable");
	//	  int n=xNode.nChildNode("NumericPredictor");
	//
	//	  // this prints the "coefficient" value for all the "NumericPredictor" tags:
	//	  for (int i=0; i<n; i++)
	//	    printf("coeff %i=%f\n",i+1,atof(xNode.getChildNode("NumericPredictor",i).getAttribute("coefficient")));
	//
	//	  // this prints a formatted output based on the content of the first "Extension" tag of the XML file:
	//	  char *t = xMainNode.getChildNode("Extension").createXMLString(true);
	//	  printf("%s\n",t);
	//
	//	  xMainNode.updateName("ROOT");
	//
	//	  xMainNode/*.getChildNode("Extension")*/.writeToFile("TesteDeSaida.xml");
	//
	//	  free(t);
	//	  return 0;
	// Teste de acesso ao XML Parser - END

//	//Teste de uso da OpenSslLib - BEGIN
//	printf("\nHASH!");
//
//	CHashUnitTest* oHashUT = new CHashUnitTest(string("LogHashUnitTest.txt"));
//	oHashUT->RunHashTest();
//	delete oHashUT;
//
//	printf("\nAES!");
//
//	CAesUnitTest* oAesUT = new CAesUnitTest(string("LogAesUnitTest.txt"));
//	oAesUT->RunAesTest();
//	delete oAesUT;
//
//	printf("\nRSA!");
//
//	CRsaUnitTest* oRsaUT = new CRsaUnitTest(string("LogRsaUnitTest.txt"));
//
//	oRsaUT->RunRSATest();
//
//	delete oRsaUT;
//    printf("\n");
	//Teste de uso da OpenSslLib - END

	CParseCmd oParseCmd;
	if (!oParseCmd.ParseCmd(argc, argv))
		return 0;

	int iPortNumber = 0;

	string sServerIP;
	sServerIP.clear();

	string sSamplingTime="1";

	string sParameter = oParseCmd.GetParameter(ID_INI_FILE);
	if( !sParameter.empty() )
	{
		CIniAccessor oIniAcessor;
		oIniAcessor.ReadIniFile( sParameter );

		sServerIP = oIniAcessor.GetKeyValue("ConnectionInfo", "IPAddress");
		string sPort = oIniAcessor.GetKeyValue("ConnectionInfo", "Port");
		iPortNumber = atoi(sPort.c_str());

		string sSamplingTime = oIniAcessor.GetKeyValue("DataManagerInfo", "SamplingTime");
		cout <<  "Sampling Time from ini file: " << sSamplingTime << endl;

		string sSecurityMode = oIniAcessor.GetKeyValue("SecurityInfo", "SafeDataTransfer");
		cout <<  "SafeDataTransfer from ini file: " << sSecurityMode << endl;
	}
	else
	{
		sParameter = oParseCmd.GetParameter(ID_IP_ADDRESS);
		if( !sParameter.empty() )
		{
			sServerIP = sParameter;
		}
		else
		{
			cout << "Missing IP Address!  Use \"-i\" argument."
					<< endl;
			return 0;
		}

		sParameter = oParseCmd.GetParameter(ID_PORT_NUMBER);
		if( !sParameter.empty() )
		{
			iPortNumber = atoi(sParameter.c_str());
		}
		else
		{
			cout << "Missing Port Address!  Use \"-p\" argument."
					<< endl;
			return 0;
		}

		sSamplingTime = oParseCmd.GetParameter(ID_TIME_SAMPLE); //optional
		//		else
		//		{
		//			cout << "Missing Sampling time!  Use \"-t\" argument."
		//				 << endl;
		//			return 0;
		//		}
		string sSecurityMode = oParseCmd.GetParameter(ID_SECURITY_MODE); //optional
		if(sSecurityMode.empty())
		{
			cout << "Using default mode: SECURITY OFF" << endl;
		}
		else
		{
			transform(sSecurityMode.begin(), sSecurityMode.end(), sSecurityMode.begin(),::toupper);
			if(sSecurityMode.compare("ON") == 0)
			{
				cout << "Using SECURITY MODE: ON" << endl;
			}
			if(sSecurityMode.compare("OFF") == 0)
			{
				cout << "Using SECURITY MODE: OFF" << endl;
			}
		}
	}

	cout << "IP: "
			<< sServerIP
			<< " Port: "
			<< iPortNumber
			<< "Sampling Time(s): "
			<< sSamplingTime
			<< endl;

	//Prepare data to send to Server
	const int iBUFFERSIZE = 16000 * atoi(sSamplingTime.c_str());
	int*  iBuffer = new int[iBUFFERSIZE];
	memset(iBuffer, 0, (iBUFFERSIZE) * sizeof(int) );

#ifdef _USE_PRU
#ifdef __LINUX
	int iLine = 0;
	//	int* iData = new int[iBUFFERSIZE];

	//Enable PRU
	cout << "Enabling PRU and ADC..."
			<< endl;
	system( "./enable_pru.sh" );
	//system( "echo BB-BONE-PRU-01 > /sys/devices/bone_capemgr.8/slots" );
	cout << "Done."
			<< endl;

	//	//Enable ADC
	//	cout << "Enabling ADC..." << endl;
	//	system( "echo cape-bone-iio > /sys/devices/bone_capemgr.*/slots" );
	//	cout << "Done." << endl;

	//Signal sampling
	cout << "Sampling signal at 16KHz"
			<< endl;
	chdir( "./testPRU/" );

	string sCommand = "./ADCCollector ";
	sCommand.append( sSamplingTime );

	//system( "./ADCCollector 1" );
	system( sCommand.c_str() );

	chdir( ".." );

	cout << endl;
	cout << "Sending data TO SERVER..."
			<< endl;

	FILE* fp = fopen("./testPRU/Results.txt", "r");

	if( !fp )
	{
		cout << "ERROR opening sample file!"
				<< endl;
		return 0;
	}

	while( iLine < 16000 * atoi(sSamplingTime.c_str()) )
		fscanf( fp, "%d", &iBuffer[iLine++]);

	fclose(fp);
	cout << "Data loaded!"
			<< endl;
#endif
#endif
	CSocketData* oSignalData = new CSocketData();

	//	const int iBUFFERSIZE = 16000 * atoi(sSamplingTime.c_str());
	char* cBuffer = new char[iBUFFERSIZE+1];
	memset(cBuffer, 0x00, iBUFFERSIZE+1 );

	//#ifdef _WINDOWS
	char index='0';
	srand (clock());
	for(int i=0; i < iBUFFERSIZE; ++i )
	{
		//int array
		int iValue = (rand() % 1800); //valid range from Beagle Bone voltage
		iBuffer[i] = iValue;

		//Char array
		cBuffer[i] = index;
		index++;
		if( index > '9' )
			index = '0';
	}
	//#endif

	oSignalData->SetCommand( COMMAND_HELLO );
	//	oSignalData->SetFrameType( TYPE_TEXT );
	oSignalData->SetFrameType( TYPE_INT_SEQUENCE );

	oSignalData->SetLongBuffer(iBuffer, iBUFFERSIZE * sizeof(int));
	//oSignalData->SetLongBuffer(cBuffer, iBUFFERSIZE);
	oSignalData->SetFrameTypeLength( iBUFFERSIZE );
	oSignalData->SetSizeOfFrameType( sizeof(int) ); //this may differ from one S.O. to another (and the element type can be different also)
	//oSignalData->SetSizeOfFrameType( sizeof(char) ); //this may differ from one S.O. to another (and the element type can be different also) - deve ser inutil

	//Log
	ofstream oFile;
	oFile.open ("ToServer.txt");
	oFile << cBuffer;
	oFile.close();

	FILE* fp1 = fopen("ToServerInt.txt", "w");
	for(int i=0; i < iBUFFERSIZE; ++i)
	{
		fprintf(fp1, "%d\n", iBuffer[i]);
	}
	//fwrite(pLongBuffer, 1, oFirstFrameData->GetTotalBytes(), fp);
	fclose(fp1);

	delete[] iBuffer;
	delete[] cBuffer;

	cout << "Size of Long" << sizeof(int) << endl;

	int NumberOfFrames = oSignalData->GetNumberOfFrames();
	cout << "Number of Frames: " << NumberOfFrames << endl;

	//Size(bytes)
	float fTotalBytes = iBUFFERSIZE * sizeof(int);
	printf( "Total data to Transfer: %ld bytes ", (long)fTotalBytes);
	if( fTotalBytes > _KB && fTotalBytes <= _MB )
		printf( "(%f KB)\n", fTotalBytes / _KB);
	else if( fTotalBytes > _MB && fTotalBytes <= _GB )
		printf( "(%f MB)\n", fTotalBytes / _MB);
	else if( fTotalBytes > _GB )
		printf( "(%f GB)\n", fTotalBytes / _GB);

	//Connect and send data to Server
	CClient* oClient = new CClient( sServerIP, iPortNumber ); //( "192.168.0.32", 23500);
	COMM_STATUS rResult = oClient->ConnectToServer();

	cout << "Register result: "
			<< oClient->CommStatusText( rResult )
			<< endl;

	oClient->SetData( oSignalData );
	rResult = oClient->CommunicateToServer();

	cout << "Result from Communicate to Server: " << oClient->CommStatusText( rResult ) << endl;

	cout << "Sizeof(int) = " << sizeof(int) << endl;

	return 0;

	//Create a thread for processing signal
	//	pthread_t thread1;
	//	int i1;
	//    i1 = pthread_create( &thread1, NULL, threadSignalSampling, (void*) NULL );
	//
	//    if( i1 )
	//    {
	//    	cout << "ERROR! Thread could not be created for signal sampling " << endl;
	//    }

	//Processamento do sinal
	Janela *janela = new Janela(64, sinal);

	//Seta dados e manda comando
	CSocketData* oData = new CSocketData();
	//	cout << "Caracteristicas" << endl;
	oData->SetData(janela->extrairCaracteristicas());

	oData->SetCommand( COMMAND_SEND_DATA );

	oClient->SetData( oData );
	rResult = oClient->CommunicateToServer();
	if( rResult == STATUS_OK )
	{
		//cout << "Data from SERVER: [" << oData->GetMessageText() << "]" << endl;
	}

	delete oClient;
	delete oData;

	return 0;
}

/*
 * Thread which handles signal sampling
 */
void* threadSignalSampling(void *obj)
{
	//All we do here is call the do_work() function
	//	CSocketManager* oManager = reinterpret_cast<CSocketManager*>(obj);
	//
	//	if( oManager )
	//	{
	//		oManager->OnHandleConnection();
	//	}

	while( true )
	{
		//call the sampling program to generate a file
		//system( ./adcsample 16khz > file );
		//transfer file to server
	}

	pthread_exit(NULL);
	return NULL;
}


