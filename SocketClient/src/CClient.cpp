/*
 * CClientWin.cpp
 *
 *  Created on: 01/05/2014
 *      Author: Rodrigo de T. Caropreso
 */

#include "CClient.h"

#include<sstream>  //for stringstream use
#include <fstream> //for ofstream use

#define _PRINT_MESSAGES_OFF

CClient::CClient(string sIP, long lPort) :
		CSocketClientBase(sIP, lPort)
{
	m_uSocket = -1;
	m_oData = NULL;
}

CClient::~CClient()
{
#ifdef __LINUX
	if (m_uSocket >= 0)
		close( m_uSocket );
#endif

#ifdef __WINDOWS
	//WinSock Cleanup
	if (m_uSocket >= 0)
		closesocket (m_uSocket);

	WSACleanup();
#endif
}

int CClient::_GetLastError()
{
#ifdef __WINDOWS
	return WSAGetLastError();
#endif

#ifdef __LINUX
	return errno;
#endif
}

COMM_STATUS CClient::ConnectToServerForWindows()
{
#ifdef __WINDOWS
	WSAData version;        //We need to check the version.
	WORD mkword = MAKEWORD(2, 2);
	int iResult = WSAStartup(mkword, &version);
	if (iResult != 0)
	{
		cout << "This version is not supported!" << _GetLastError()
		     << endl;
		return STATUS_CONNECTION_ERROR;
	}
	else
	{
		cout << "Winsock version OK!"
			 << endl;
	}

	m_uSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_uSocket == INVALID_SOCKET)
	{
		cout << "Socket creation FAILED"
			 << endl;
		return STATUS_SOCKET_CREATION_ERROR;
	}
	else
	{
		cout << "Socket creation SUCCESSFULL"
			 << endl;
	}

	//Socket address information
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(m_sIPAddress.c_str());
	addr.sin_port = htons(m_lPortNumber);
	/*==========Addressing finished==========*/

	//Now we connect
	cout << "Connecting..."
	     << endl;
	int conn = connect(m_uSocket, (SOCKADDR*) &addr, sizeof(addr));
	if (conn == SOCKET_ERROR)
	{
		cout << "Connection ERROR - "
			 << _GetLastError()
			 << endl;
		closesocket (m_uSocket);
		WSACleanup();

		return STATUS_CONNECTION_ERROR;
	}
	else
	{
		cout << "Connection SUCCESSULL on Server!"
			 << endl;
	}
#endif

	return STATUS_OK;
}

COMM_STATUS CClient::ConnectToServerForLinux()
{
#ifdef __LINUX
		m_uSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (m_uSocket < 0)
		{
			cout << "Socket creation FAILED"
			     << endl;
			return STATUS_SOCKET_CREATION_ERROR;
		}
		else
		{
			cout << "Socket creation SUCCESSFULL"
				 << endl;
		}

		struct sockaddr_in addr;
		memset(&addr, 0, sizeof(addr));

		 if(inet_pton(AF_INET, m_sIPAddress.c_str(), &addr.sin_addr)<=0)
		 {
			cout << "ERROR on address conversion."
				 << endl;
			close( m_uSocket );
			return STATUS_CONNECTION_ERROR;
		 }

		//Fill Address info
		addr.sin_family = AF_INET;
		addr.sin_port = htons(m_lPortNumber);

		/*==========Addressing finished==========*/

		//Now we connect
		cout << "Connecting..."
			 << endl;

		int iResult = connect( m_uSocket, (sockaddr *) &addr, sizeof(addr));

		if (iResult < 0 )
		{
			cout << "Connection ERROR - "
				 << _GetLastError()
				 << endl;
			close( m_uSocket );
			return STATUS_CONNECTION_ERROR;
		}
		else
		{
			cout << "Connection SUCCESSULL on Server!"
				 << endl;
		}

		return STATUS_OK;
#endif
	return STATUS_OK;
}

COMM_STATUS CClient::ConnectToServer()
{
#ifdef __WINDOWS
	return ConnectToServerForWindows();
#endif

#ifdef __LINUX
	return ConnectToServerForLinux();
#endif
}

void CClient::SetData(CSocketData* oData)
{
	m_oData = oData;
}

CSocketData* CClient::GetData()
{
	return m_oData;
}

COMM_STATUS CClient::CommunicateToServer()
{
	cout << "    *Communication with server: BEGIN"
	     << endl;


	clock_t t_begin = 0, t_end = 0;

	string sMessage = "";
	ostringstream strStream(ostringstream::ate);
	int iTotalErrorFrames = 0;
	COMM_STATUS bRet = STATUS_OK;

	int iNumberOfFrames = m_oData->GetNumberOfFrames();
	int iBytesReceived=0;

	CSocketData* oDataFromServer = new CSocketData(); //to check received data from server

	int iBytesToSend = CSocketData::GetFrameLength(); //Fixed size
	byte* pBuffer = new byte[iBytesToSend];

	m_oData->ResetFrameIndex();

	strStream << "Total Frames: "
			  << iNumberOfFrames
			  << endl;

	t_begin = clock();
	cout << "Begin: " << t_begin << endl;

	for( int i=0; i < iNumberOfFrames; ++i )
	{
		//On the end of last iteration, we received the Status from Server
		//FAIL SIMULATION - BEGIN
		if( bRet == STATUS_DATA_CORRUPTED )
		{
			//a message can be logged here indicating the error
			strStream << "Error on Frame:"
					  << i
					  << " Total Fails: "
					  << ++iTotalErrorFrames
					  << endl;
		}
		//FAIL SIMULATION - END

		#ifdef _PRINT_MESSAGES_ON
			cout << "    *Sending FRAME: "
			     << i
			     << endl;
		#endif

		//If last frame status was OK, try to send next frame
		//If some error ocurred, let's try to send the same frame (by now it's sync communication)
		if( bRet == STATUS_SERVER_ACK || bRet == STATUS_OK )
		{
			if( m_oData->SetNextFrameForTransfer() == END_OF_FRAMES )
				break;
		}

		//Build struct
		m_oData->SetFrameStatus(STATUS_OK);

		//Transfer to buffer
		if( !m_oData->GetFrame( pBuffer, iBytesToSend ) )
		{
			bRet = STATUS_SEND_ERROR;
			goto FINISH;
		}

		//Send Frame to Server
		iBytesReceived = 0;
		iBytesReceived = send( m_uSocket, (char*) pBuffer, iBytesToSend, 0 );

		#ifdef _PRINT_MESSAGES_ON
			cout << "    *Data to server:   "
			     << "Sent bytes: "
			     << iBytesToSend
			     << " Received bytes: "
			     << iBytesReceived
			     << endl;
		#endif

		if(iBytesReceived != iBytesToSend)
		{
			#ifdef _PRINT_MESSAGES_ON
				#ifdef __WINDOWS
						cout << "    *Error in Sending: "
						     << WSAGetLastError()
						     << endl;
				#endif

				#ifdef __LINUX
						cout << "    *Error in Sending: "
						     << errno
						     << endl;
				#endif
			#endif

			bRet = STATUS_SEND_ERROR;
			goto FINISH;
		}
		else
		{
			memset(pBuffer, 0, iBytesToSend); //clear pBuffer

			//Receive data from server
			iBytesReceived = 0;

			iBytesReceived = recv(m_uSocket, (char*) pBuffer, iBytesToSend, 0);

			#ifdef _PRINT_MESSAGES_ON
				cout << "    *Data from server: "
					 << "Sent bytes: "
					 << iBytesToSend
					 << " Received bytes: "
					 << iBytesReceived
					 << endl;
			#endif

			if(iBytesReceived == 0)
			{
				#ifdef _PRINT_MESSAGES_ON
					#ifdef __WINDOWS
								cout << "    *Error in Receiving: "
									 << _GetLastError()
									 << endl;
					#endif

					#ifdef __LINUX
								cout << "    *Error in Receiving: "
									 << _GetLastError()
									 << endl;
					#endif
				#endif

				bRet = STATUS_RECEIVE_ERROR;
				goto FINISH;
		    }
			else
			{
				//Just check returned data
				oDataFromServer->ClearFrame();
				oDataFromServer->SetFrame(pBuffer, iBytesReceived);

				bRet = oDataFromServer->GetFrameStatus(); //get status of the operation

				#ifdef _PRINT_MESSAGES_ON
					cout << "Status received from Server: "
						 << CSocketData::GetCommStatusText(bRet)
						 << endl;
				#endif

				if( bRet == STATUS_DATA_CORRUPTED )
				{
					i--; //restore index position (by now it's sync, so it's the previous frame which failed)
					continue;
				}
			}
		}
	}//for(i)

	FINISH:
	t_end = clock();
	cout << "End : " << t_end << endl;

	delete []pBuffer;
	delete oDataFromServer;

	//CLOCKS_PER_SEC (CPS): on Windows, CPS = 1000, on BBB CPS = 1000000
	float fElapsed = (((float)(t_end - t_begin)) / CLOCKS_PER_SEC) * 1000;

	cout << "    *Communication with server: END - Status: "
		 << CSocketData::GetCommStatusText(bRet)
		 << " Elapsed Time(ms): "
		 << fElapsed
		 << endl;

	//Log Fail results
	ofstream ofs ("FailResults.txt", ofstream::out);
	ofs << strStream.str();
	ofs.close();

	return bRet;
}

string CClient::CommStatusText(COMM_STATUS Result)
{
	switch(Result)
	{
		case STATUS_OK:
			return "OK";
		case STATUS_REG_ERROR:
			return "REGISTER ERROR";
		case STATUS_CONNECTION_ERROR:
			return "CONNECTION ERROR";
		case STATUS_SOCKET_CREATION_ERROR:
			return "SOCKET CREATION ERROR";
		case STATUS_RECEIVE_ERROR:
			return "RECEIVE DATA ERROR";
		case STATUS_SEND_ERROR:
			return "SEND DATA ERROR";
		case STATUS_CLIENT_ACK:
			return "CLIENT ACKNOWLEDGED DATA";
		case STATUS_SERVER_ACK:
			return "SERVER ACKNOWLEDGED DATA";
		default:
			return "UNKNOWN STATUS";
	}
}

