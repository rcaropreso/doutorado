/*
 * CSocketData.h
 *
 *  Created on: 03/05/2014
 *      Author: Rodrigo
 */

#ifndef CSOCKETDATA_H_
#define CSOCKETDATA_H_

#include <iostream>

#include <string>
#include <vector>

using namespace std;

typedef unsigned char byte;
//typedef int REGISTER_RESULT;

#define MAX_REG_STR 64
#define MAX_BUFFER  1024

#define END_OF_FRAMES         -2

/*
 * Enumerations
 */
typedef enum __DATA_TYPES_61850
{
	TYPE_UNDEFINED=0,
	TYPE_TEXT,
	TYPE_SEQUENCE,
	TYPE_INT_SEQUENCE,
	TYPE_END
} DATA_TYPES_61850;

typedef enum __COMM_STATUS
{
	STATUS_OK=0,
	STATUS_REG_ERROR,              //1
	STATUS_CONNECTION_ERROR,       //2
	STATUS_SOCKET_CREATION_ERROR,  //3
	STATUS_RECEIVE_ERROR,          //4
	STATUS_SEND_ERROR,             //5
	STATUS_CLIENT_ACK,             //6
	STATUS_SERVER_ACK,             //7
	STATUS_DATA_CORRUPTED,         //8
	STATUS_END                     //9
}COMM_STATUS;

typedef enum __COMM_COMMANDS
{
	COMMAND_UNDEFINED = 0,
	COMMAND_HELLO,
	COMMAND_SEND_DATA,
	COMMAND_CLOSE_SERVER
}COMM_COMMANDS;

typedef struct _SOCKET_DATA_61850
{
	unsigned int     iCommand;
	COMM_STATUS      iStatus;
	DATA_TYPES_61850 FrameType;
	int              iFrameIndex;
	int              iTotalFrames;
	long             lTotalFrameTypeLength; //total length of the elements which type is given by FrameType
	int              iSizeOfFrameType; //size (bytes) of each element which type is given by FrameType
	long             lTotalBytes; //total bytes for all frames to transfer
	long             DataBufferLength;
	char             pDataBuffer[MAX_BUFFER];
}SOCK_DATA_61850;

class CSocketData
{
public:
	CSocketData();
	CSocketData(const SOCK_DATA_61850* pData);
	virtual ~CSocketData();

	void SetData(double* dbData);
	void SetData(int* iData);
	void SetDataBlockIndex(int iDataBlockIndex );
	void SetDataBlockLength(int iDataBlockLength );

	double* GetDoubleData();

	int GetDataBlockLength();
	int GetDataBlockIndex();
	void GetData( int* cData );

	//------// New interface

	void ClearFrame();

	static string GetCommStatusText(COMM_STATUS Status);

	//int ChangeFrameData( void* pBuffer, unsigned int iBufferLength, DATA_DIRECTION Direction ); //the SOCK_DATA_61850 to be sent on the ethernet
	int SetFrame( void* pBuffer, unsigned int iBufferLength );
	int GetFrame( void* pBuffer, unsigned int iBufferLength );

	int SetLongBuffer( void* pBuffer, unsigned long BufferLength );

	int SetNextFrameForTransfer();

	void SetCommand( int iCommand );
	int SetFrameType( DATA_TYPES_61850 Type );
	int SetFrameStatus( COMM_STATUS Status);

	void SetFrameTypeLength( long lFrameTypeLength) { m_pData.lTotalFrameTypeLength = lFrameTypeLength; };
	void SetSizeOfFrameType( int  iSizeOfFrameType) { m_pData.iSizeOfFrameType = iSizeOfFrameType; };


	int  GetCommand()                { return m_pData.iCommand;    };
	DATA_TYPES_61850 GetFrameType()  { return m_pData.FrameType;   };
	COMM_STATUS GetFrameStatus()     { return m_pData.iStatus;     };
	int   GetFrameIndex()            { return m_pData.iFrameIndex; };
	int   GetNumberOfFrames()        { return m_pData.iTotalFrames;};
	long  GetTotalBytes()            { return m_pData.lTotalBytes; };
	long  GetDataLength()            { return m_pData.DataBufferLength; };
	char* GetData()                  { return m_pData.pDataBuffer; };

	long GetFrameTypeLength()        { return m_pData.lTotalFrameTypeLength; };
	long GetSizeOfFrameType()        { return m_pData.iSizeOfFrameType; };

	void SetFrameIndex( int iFrameIndex ) {m_pData.iFrameIndex = iFrameIndex;};

	void ResetFrameIndex()           {m_pData.iFrameIndex = -1;};

	static int GetFrameLength()             { return sizeof(SOCK_DATA_61850); };

	void InitializeData();
	void _LogTransferData();
private:
	//Olddata - begin
	double m_dbData[13];
	int    m_iDataObj[MAX_BUFFER];
	int m_iDataBlockIndex;
	int m_iDataBlockLength;
	//old data - end

	void* m_pLongBuffer;
	//long  m_LongBufferLength;

	//Receive from Server (RX)
	//TBI
	SOCK_DATA_61850 m_pData;
};

#endif /* CSOCKETDATA_H_ */
