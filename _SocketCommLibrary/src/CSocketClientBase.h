/*
 * CSocketClientBase.h
 *
 *  Created on: 17/05/2014
 *      Author: Rodrigo
 */

#ifndef CSOCKETCLIENTBASE_H_
#define CSOCKETCLIENTBASE_H_

#include <string>
#include <iostream>

#include "CSocketData.h"

using namespace std;

class CSocketClientBase
{
public:
	CSocketClientBase(string sIP, long lPort);
	virtual ~CSocketClientBase();

	virtual COMM_STATUS ConnectToServer() = 0;
	virtual void SetData(CSocketData* oData) = 0;
	virtual COMM_STATUS CommunicateToServer() = 0;

	virtual void CloseConnection();
	virtual void DisplayMessage( char* sMessage );

	string GetIPAddress(){ return m_sIPAddress; };
	long   GetPortNumber(){ return m_lPortNumber; };

	unsigned int GetSocket() {return m_uSocket;};

protected:
	string m_sIPAddress;
	long m_lPortNumber;
	unsigned int m_uSocket;

	CSocketData* m_oData; //data to be exchanged on Server
};

#endif /* CSOCKETCLIENTBASE_H_ */
