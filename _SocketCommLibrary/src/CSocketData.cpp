/*
 * CSocketData.cpp
 *
 *  Created on: 03/05/2014
 *      Author: Rodrigo
 */

#include "CSocketData.h"
#include <string.h>

#define IS_VALID_TYPE( Type ) ( Type >= TYPE_TEXT && Type < TYPE_END )
#define IS_VALID_STATUS( Status ) ( Status >= STATUS_OK && Status < STATUS_END )

CSocketData::CSocketData()
{
	// TODO Auto-generated constructor stub
	InitializeData();
}

CSocketData::CSocketData(const SOCK_DATA_61850* pData)
{
	InitializeData();

	if( pData )
		memcpy((void*)&m_pData, (void*)pData, sizeof(SOCK_DATA_61850));
}

string CSocketData::GetCommStatusText(COMM_STATUS Status)
{
	switch(Status)
	{
		case STATUS_OK:
			return "STATUS_OK";
		case STATUS_REG_ERROR:
			return "STATUS_REG_ERROR";
		case STATUS_CONNECTION_ERROR:
			return "STATUS_CONNECTION_ERROR";
		case STATUS_SOCKET_CREATION_ERROR:
			return "STATUS_SOCKET_CREATION_ERROR";
		case STATUS_RECEIVE_ERROR:
			return "STATUS_RECEIVE_ERROR";
		case STATUS_SEND_ERROR:
			return "STATUS_SEND_ERROR";
		case STATUS_CLIENT_ACK:
			return "STATUS_CLIENT_ACK";
		case STATUS_SERVER_ACK:
			return "STATUS_SERVER_ACK";
		case STATUS_DATA_CORRUPTED:
			return "STATUS_DATA_CORRUPTED";
		case STATUS_END:
			return "STATUS_END";
		default:
			return "UNKNOWN STATUS";
	}

	return "";
}

void CSocketData::InitializeData()
{
	ClearFrame();

	m_pLongBuffer = NULL;

	if(m_pLongBuffer)
	{
		delete[] (char*)m_pLongBuffer;
		m_pLongBuffer=NULL;
	}
}

CSocketData::~CSocketData()
{
	// TODO Auto-generated destructor stub
	if(m_pLongBuffer)
	{
		delete[] (char*)m_pLongBuffer;
		m_pLongBuffer=NULL;
	}
}

void CSocketData::SetCommand( int iCommand )
{
	m_pData.iCommand = iCommand;
}

double* CSocketData::GetDoubleData()
{
	return m_dbData;
}

void CSocketData::SetData( double* dbData )
{
	for(int i=0; i < 13; ++i)
	{
		m_dbData[i] = dbData[i];
	}
}

void CSocketData::GetData( int* cData )
{
	for( long i=0; i < MAX_BUFFER; ++i )
		cData[i] = m_iDataObj[i];
}

void CSocketData::SetData( int* iData )
{
	for(long i=0; i < m_iDataBlockLength; ++i)
		m_iDataObj[i] = iData[i];
}

void CSocketData::SetDataBlockIndex(int iDataBlockIndex )
{
	m_iDataBlockIndex = iDataBlockIndex;
}

int CSocketData::GetDataBlockIndex()
{
	return m_iDataBlockIndex;
}

void CSocketData::SetDataBlockLength(int iDataBlockLength )
{
	m_iDataBlockLength = iDataBlockLength;
}

int CSocketData::GetDataBlockLength()
{
	return m_iDataBlockLength;
}

int CSocketData::SetFrameType( DATA_TYPES_61850 Type )
{
	if( !IS_VALID_TYPE( Type ) )
		return false;

	m_pData.FrameType = Type;

	return true;
}

int CSocketData::SetFrame( void* pBuffer, unsigned int iBufferLength )
{
	if( iBufferLength < sizeof(SOCK_DATA_61850) || pBuffer == NULL )
		return false;

	//Clear destination buffer
	memset( (void*) &m_pData, sizeof(SOCK_DATA_61850), 0x00);

	//Import data to internal buffer
	memcpy( (void*) &m_pData, pBuffer, sizeof(SOCK_DATA_61850) );

	return true;
}

int CSocketData::GetFrame( void* pBuffer, unsigned int iBufferLength )
{
	if( iBufferLength < sizeof(SOCK_DATA_61850) || pBuffer == NULL )
		return false;

	//Clear destination buffer
	memset(pBuffer, iBufferLength, 0x00);

	//Export data from internal buffer
	memcpy( pBuffer, (void*) &m_pData, sizeof(SOCK_DATA_61850) );

	return true;
}

int CSocketData::SetFrameStatus( COMM_STATUS Status )
{
	if( !IS_VALID_STATUS( Status ) )
		return false;

	m_pData.iStatus = Status;

	return true;
}

void CSocketData::ClearFrame()
{
	memset( &m_pData, 0x00, sizeof(SOCK_DATA_61850) );
}

int CSocketData::SetLongBuffer( void* pBuffer, unsigned long BufferLength )
{
	if( pBuffer == NULL )
		return false;

	m_pLongBuffer = new char[BufferLength];
	memset( m_pLongBuffer, 0x00, BufferLength );
	memcpy( m_pLongBuffer, pBuffer, BufferLength );

	//Set Number of Frames necessary to send the buffer passed as parameter
	//Estes dados deveriam ficar em outro metodo(ou entao deve-se criar um metodo para copiar do m_pData para o LongBuffer
	m_pData.lTotalBytes   = BufferLength;
	m_pData.iTotalFrames  = BufferLength / MAX_BUFFER;
	m_pData.iTotalFrames += ((BufferLength % MAX_BUFFER) == 0 ? 0 : 1);

	ResetFrameIndex(); //reset frame index

	cout << "End of LongBuffer Setup - Number of Frames=" << m_pData.iTotalFrames << " LongBuffer Length=" << BufferLength << endl;

	return true;
}

int CSocketData::SetNextFrameForTransfer()
{
	if( m_pData.iFrameIndex < 0 )
	{
		//First Frame
	}

	if( m_pData.iFrameIndex == (m_pData.iTotalFrames - 1) )
	{
		//Last frame,no more frames to go
		return END_OF_FRAMES;
	}

	m_pData.iFrameIndex++;

	//Copy next frame to the transfer buffer
	memset( m_pData.pDataBuffer, 0x00, MAX_BUFFER );

	if( (m_pData.iFrameIndex + 1)* MAX_BUFFER < m_pData.lTotalBytes )
	{
		m_pData.DataBufferLength = MAX_BUFFER;
	}
	else
	{
		//Last frame usually has less than MAX_BUFFER bytes
		m_pData.DataBufferLength = m_pData.lTotalBytes - (m_pData.iFrameIndex * MAX_BUFFER);
	}

	//Copy buffer
	long lOffSet = m_pData.iFrameIndex * MAX_BUFFER;
	memcpy(m_pData.pDataBuffer, ((char*)m_pLongBuffer) + lOffSet, m_pData.DataBufferLength);

	return m_pData.iFrameIndex;
}

void CSocketData::_LogTransferData()
{
	//Just log
	cout << "TransferData.iCommand:     " << m_pData.iCommand         << endl;
	cout << "TransferData.BufferType:   " << m_pData.FrameType        << endl;
	cout << "TransferData.iStatus:      " << m_pData.iStatus          << endl;
	cout << "TransferData.iFrameIndex:  " << m_pData.iFrameIndex      << endl;
	cout << "TransferData.iTotalFrames: " << m_pData.iTotalFrames     << endl;
	cout << "TransferData.BufferLength: " << m_pData.DataBufferLength << endl;

	char AuxBuffer[MAX_BUFFER+1];
	memset(AuxBuffer, 0x00, MAX_BUFFER+1);
	memcpy(AuxBuffer, m_pData.pDataBuffer, m_pData.DataBufferLength);
	cout << "TransferData.Buffer:"        << AuxBuffer                << endl;
}
