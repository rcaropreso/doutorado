//============================================================================
// Name        : ADConverterTest.cpp
// Author      : Rodrigo Caropreso
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sstream>

#include <fcntl.h>
#include <stdlib.h>

using namespace std;

#define _AIN0 "AIN0"
#define _AIN1 "AIN1"
#define _AIN2 "AIN2"
#define _AIN3 "AIN3"
#define _AIN4 "AIN4"
#define _AIN5 "AIN5"
#define _AIN6 "AIN6"
#define _AIN7 "AIN7"

int main() {

	//para ligar o ADC:
	//echo cape-bone-iio > /sys/devices/bone_capemgr.*/slots

	time_t start_t, end_t;
	double diff_t;

//	FILE *aval;
	//FILE *aval1;
//	int value;
	//int value1;
	int i, j;

//	int iValues[35000]; //para guardar 1 segundo (30Khz aprox.)
	int iValueIndex = 0;

	cout << "Reading Analog Inputs" << endl;
	ostringstream strStream, ss;

	strStream << "/sys/devices/ocp.2/helper.14/" ;
	strStream << _AIN0;

	//ss << "cat " << strStream.str() << " >> signal.out" << endl;

	printf("Starting loop...\n");
	time(&start_t);

	int fd;
	int ch[30000][5];

//	cout << ss.str().c_str() << endl;
//	system( ss.str().c_str() );

	for(int i=0; i < 30000; ++i )
		ch[i][0] = ch[i][1] = ch[i][2] = ch[i][3] = ch[i][4] = '\0';

	fd = open(strStream.str().c_str(), O_RDONLY);
	while(iValueIndex < 7500)
	{
		//aval = fopen("/sys/devices/ocp.2/helper.14/AIN0", "r");
		//aval = fopen(strStream.str().c_str(), "r");
		//system( ss.str().c_str() );
		//iValueIndex++;
		read(fd, ch[iValueIndex++], 4);
		lseek(fd, 0, 0);
		//printf("%s\n",ch[iValueIndex++]);

		//fseek ( aval, 0, SEEK_SET );
		//fscanf( aval, "%d", &value );
		//fclose( aval );

		//for(i = 0; i<1000000;i++);
		usleep( 1 );
		//iValues[iValueIndex++] = value;

//		time(&end_t);
//		if( difftime(end_t, start_t) == 1)
//			break;

		//printf( "value: %d:\n", value );

//		aval1 = fopen("/sys/devices/ocp.2/helper.14/AIN1","r");
//		fseek(aval1,0,SEEK_SET);
//		fscanf(aval1,"%d",&value1);
//		fclose(aval1);
//		printf("value: %d  value1: %d\n",value,value1);
//		for(i = 0; i<1000000;i++);
	}
	close(fd);

	time(&end_t);
	diff_t = difftime(end_t, start_t);

	FILE* fOut = fopen("signal.out", "w");
	iValueIndex--;
	for( i=0; i < iValueIndex; i++ )
		for( j=0; j < 5; j++ )
		{
			fprintf(fOut, "%d\n", ch[i][j]);//iValues[i]);
		}

	fclose(fOut);

	printf("Execution time = %f [%d] - Avg = %f samples/s\n", diff_t, iValueIndex+1, (iValueIndex+1) / diff_t );
	printf("Exiting of the program...\n");

	return 0;
}
