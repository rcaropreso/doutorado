:: Author: Rodrigo de Toledo Caropreso
:: Data: 2016/02/18
:: Copy dependency files of the project to OUTPUT_FOLDER

:: copymodules [BuildType]
:: the parameter [Buildtype] MUST match the OUTPUT_FOLDER of the project

@echo off

echo.
echo.
set WORKSPACE=D:\_Doutorado\workspace

set LIBSOCKETCOMMDIR=%WORKSPACE%\_SocketCommLibrary
set LIBSIGPROCESSDIR=%WORKSPACE%\_SigProcessLibrary
set LIBCOMMONDIR=%WORKSPACE%\_CommonLibrary
set LIBSECURITYDIR=%WORKSPACE%\_SecurityLibrary

if [%1] == [] goto ERROR
if  %1  == windows goto WIN
if  %1  == linux   goto LINUX

set OUTPUT_FOLDER=%1

if %1==DebugWindows (
	goto WIN
	)

if %1==ReleaseWindows (
	goto WIN
	)

if %1==DebugLinuxARM (
	goto LINUX
	)

if %1==ReleaseLinuxARM (
	goto LINUX
	)

:: ERROR invalid parameter
goto ERROR

:WIN
echo ***Copying MinGW redistributable files...

copy %MINGW_HOME%\bin\libgcc_s_dw2-1.dll %PWD%
copy "%MINGW_HOME%\bin\libstdc++-6.dll" %PWD%

echo ***Copying Library Files for WINDOWS...
copy %LIBSOCKETCOMMDIR%\%OUTPUT_FOLDER%\*.dll %PWD%
copy %LIBSIGPROCESSDIR%\%OUTPUT_FOLDER%\*.dll %PWD%
copy %LIBCOMMONDIR%\%OUTPUT_FOLDER%\*.dll %PWD%
copy %LIBSECURITYDIR%\%OUTPUT_FOLDER%\*.dll %PWD%
copy %WORKSPACE%\pthreads\dll\x86\pthreadGCE2.dll %PWD%

goto COMMON

:LINUX
echo ***Copying Library Files for LINUX...
copy %LIBSOCKETCOMMDIR%\%OUTPUT_FOLDER%\*.so %PWD%
copy %LIBSIGPROCESSDIR%\%OUTPUT_FOLDER%\*.so %PWD%
copy %LIBCOMMONDIR%\%OUTPUT_FOLDER%\*.so %PWD%
copy %LIBSECURITYDIR%\%OUTPUT_FOLDER%\*.so %PWD%
copy %WORKSPACE%\pthreads\lib\x86\libpthreadGCE2.a %PWD%

goto COMMON

:COMMON
echo ***Copying pthreads redistributable files...

goto END

: ERROR
echo Usage: copymodules <SO_TARGET>
goto END

:END