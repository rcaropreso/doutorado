/*
 * CSocketClientBase.cpp
 *
 *  Created on: 17/05/2014
 *      Author: Rodrigo
 */

#include "CSocketClientBase.h"

CSocketClientBase::CSocketClientBase(string sIP, long lPort) :
		m_sIPAddress(sIP), m_lPortNumber(lPort)
{
	m_uSocket = -1;
	m_oData = NULL;
}

CSocketClientBase::~CSocketClientBase()
{
	// TODO Auto-generated destructor stub
	CloseConnection();
}

void CSocketClientBase::CloseConnection()
{
	//ASSERT(0);
}

void CSocketClientBase::DisplayMessage( char* sMessage )
{

}
