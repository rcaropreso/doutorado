#include "CHashProxy.h"

CHashProxy::CHashProxy()
{

}

int CHashProxy::GetAllocationSizeForSHA256()
{
    return EVP_MAX_MD_SIZE;
}

bool CHashProxy::GetDigestSHA256(unsigned char* strHashCode)
{
   if(strHashCode == NULL)
       return false;

   memset((char*) strHashCode, 0, EVP_MAX_MD_SIZE);
   memcpy(strHashCode, m_DigestSHA256, EVP_MAX_MD_SIZE);

   return true;
}

bool CHashProxy::GenerateSHA256(const unsigned char* strPlainText, unsigned int iPlainTextLength)
{
    if(strPlainText == NULL || iPlainTextLength == 0)
        return false;

    OpenSSL_add_all_digests();

    const EVP_MD* md = EVP_get_digestbyname("sha256");

    EVP_MD_CTX context;
    EVP_MD_CTX_init(&context);
    EVP_DigestInit_ex(&context, md, NULL);
    EVP_DigestUpdate(&context, (unsigned char *)strPlainText, iPlainTextLength);

    unsigned int digestSz;
    EVP_DigestFinal_ex(&context, m_DigestSHA256, &digestSz);
    EVP_MD_CTX_cleanup(&context);

    EVP_cleanup();

    return true;
}

int CHashProxy::GetAllocationSizeForMD5()
{
    return MD5_DIGEST_LENGTH;
}

bool CHashProxy::GetDigestMD5(unsigned char* strHashCode)
{
   if(strHashCode == NULL)
       return false;

   memset((char*) strHashCode, 0, MD5_DIGEST_LENGTH);
   memcpy(strHashCode, m_DigestMD5, MD5_DIGEST_LENGTH);

   return true;
}

bool CHashProxy::GenerateMD5(const unsigned char* strPlainText, unsigned int iPlainTextLength)
{
    if(strPlainText == NULL || iPlainTextLength == 0)
        return false;

    MD5_CTX c;

    memset(m_DigestMD5, 0, MD5_DIGEST_LENGTH);

    MD5_Init(&c);
    MD5_Update(&c, strPlainText, iPlainTextLength);
    MD5_Final(m_DigestMD5, &c);

    return true;
}
