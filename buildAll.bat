::Date: 2016/02/17
::Author: Rodrigo de Toledo Caropreso
::Build All Applications from the Project

::@echo "Usage: buildAll [all | DebugWin | DebugARM | ReleaseWin | ReleaseARM] [clean] [cleanmakefiles]"
:: all            -> Build All Project components for DebugWin, ReleaseWin, DebugARM or ReleaseARM targets
:: clean          -> clean up all output folders for DebugWin, ReleaseWin, DebugARM or ReleaseARM targets
:: cleanmakefiles -> remove makefiles from 'src' folders (the makefiles remain on "makes" folders)

::example: buildAll DebugWin (build whole project for DebugWin target)

@echo off
cls

::additional components must be added to the list below
set PROJECT_COMPONENTS=_SecurityLibrary, _CommonLibrary, _SigProcessLibrary, _SocketCommLibrary, SocketClient, SocketServer

::the current workspace folder of the project mus be set below. 
set WORKSPACE="D:\_Doutorado\workspace"

if '%1'=='' (
	goto showHelp)

if %1==clean (
	 set OPERATION=clean
	 goto processOperation
	 )

if %1==cleanmakefiles (
	 set OPERATION=cleanmakefiles
	 goto processOperation
	 )

if %1==all (
	 set OPERATION=%1
	 goto processOperation
	  )

if %1==DebugWin (
	 set OPERATION=%1
	 goto processOperation
	  )

if %1==ReleaseWin (
	 set OPERATION=%1
	 goto processOperation
	  )

if %1==DebugARM (
	 set OPERATION=%1
	 goto processOperation
	  )

if %1==ReleaseARM (
	 set OPERATION=%1
	 goto processOperation
	  )

:processOperation

for %%G IN (%PROJECT_COMPONENTS%) DO ( 
	echo.
	echo ***************************************************************************
	echo Processing %WORKSPACE%/%%G/ %OPERATION%...	
	echo ***************************************************************************
	call BuildOne %WORKSPACE%/%%G/ %OPERATION%
	)

goto END

:showHelp

echo.
@echo ***This batch process performs full project clean up and/or build***
@echo "Usage: buildAll [all | DebugWin | DebugARM | ReleaseWin | ReleaseARM] [clean] [cleanmakefiles]"

:END
cd /D %WORKSPACE%
