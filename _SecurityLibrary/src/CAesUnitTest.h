#ifndef CAESUNITTEST_H
#define CAESUNITTEST_H

#include "CAesProxy.h"
#include "CSecurityCommonDefs.h"

class CAesUnitTest
{
public:
    CAesUnitTest();
    CAesUnitTest(string strFileName);
    ~CAesUnitTest();

    void RunAesTest();

private:
    void LogOutput(char* strMessage, int iMessageLength);

    bool RunAES128KeysTest(CAesProxy* oAES);
    bool RunAES128EncryptionTest(CAesProxy* oAES);

    bool RunAES256KeysTest(CAesProxy* oAES);
    bool RunAES256EncryptionTest(CAesProxy* oAES);

    _LOG_OUTPUT_TYPE m_LogType;
    FILE* m_fpLogFile;
};

#endif // CAESUNITTEST_H
