/*
 * CSocketManager.h
 *
 *  Created on: 03/05/2014
 *      Author: Rodrigo
 */

/*
 * Esta classe vai gerenciar uma conexao do CServer
 */

#ifndef CSOCKETMANAGER_H_
#define CSOCKETMANAGER_H_

//#include <string>
#include <iostream>
#include <sstream>

#include "string.h"
#include <fstream>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __WINDOWS
#include "winsock.h" //BY NOW, THIS REMOVES SOME 'UNRESOLVED SYMBOLS' FROM ECLIPSE

#include "winsock2.h"
#include <stdio.h>
#endif

#ifdef __LINUX
#include <errno.h>
#include <sys/socket.h>

#define SOCKET_ERROR -1

typedef unsigned int SOCKET;
#endif

using namespace std;

class CSocketManager
{
public:
	CSocketManager(SOCKET sock, unsigned int threadId);
	virtual ~CSocketManager();

	virtual int OnHandleConnection();

	void SetMessage( string sMessage );

	void CloseConnection();

	SOCKET GetSocket();

private:
	SOCKET m_uSocket;
	unsigned int m_iThreadId;

	string m_sMessage;
	int _GetLastError();

//	//Depois isso tem que sair daqui
//	int iDataBuffer[16000];
};

#endif /* CSOCKETMANAGER_H_ */
