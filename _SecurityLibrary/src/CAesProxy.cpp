#include "CAesProxy.h"

#define IS_VALID_ENC_TYPE(Type)  (Type == AES128 || Type == AES256)
#define AES_BLOCK_SIZE 16

CAesProxy::CAesProxy()
{
    m_CurrentEncryptionType = AES128;
    m_strKey = NULL;
    m_strInitialVector = NULL;
    m_ErrorType = NO_ERROR_REPORTED;

    //Initialize the library
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    OPENSSL_config(NULL);
}

CAesProxy::CAesProxy(_ENC_TYPE encryptionType, unsigned char* const strKey,
                     unsigned char* const strInitialVector)
{
    if(!IS_VALID_ENC_TYPE(encryptionType))
    {
        m_CurrentEncryptionType = AES128;
    }
    else
    {
        m_CurrentEncryptionType = encryptionType;
    }

    if(!AllocateKeys())
    {
        printf("Error on key memory allocation");
    }

    int iByteSize = GetByteSizeOfKey(GetCurrentEncryptionType());
    memcpy(m_strKey, strKey, iByteSize);

    iByteSize = GetByteSizeOfIV(GetCurrentEncryptionType());
    memcpy(m_strInitialVector, strInitialVector, iByteSize);

    m_ErrorType = NO_ERROR_REPORTED;

    //Initialize the library
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    OPENSSL_config(NULL);
}

CAesProxy::~CAesProxy()
{
    ReleaseMemory();

    EVP_cleanup();
    ERR_free_strings();
}

//******************Public Methods******************//
_ENC_TYPE CAesProxy::GetCurrentEncryptionType()
{
    return m_CurrentEncryptionType;
}

bool CAesProxy::GenerateKeys(_ENC_TYPE encryptionType)
{
    if(!IS_VALID_ENC_TYPE(encryptionType))
    {
        m_ErrorType = INVALID_PARAMETER;
        return false;
    }

    //Set internal state.
    m_CurrentEncryptionType = encryptionType;

    if(!AllocateKeys())
    {
        m_ErrorType = MEMORY_ALLOCATION_ERROR;
        return false;
    }

    int iKeyLength = 0;
    int iIVLength = 0;

    //Generate keys
    switch(m_CurrentEncryptionType)
    {
    case AES128:
        iKeyLength = KEY_LENGTH_AES128;
        iIVLength  = IV_LENGTH_AES128;
        break;
    case AES256:
        iKeyLength = KEY_LENGTH_AES256;
        iIVLength  = IV_LENGTH_AES256;
        break;
    }

    if (!RAND_bytes(m_strKey, iKeyLength))
    {
        m_ErrorType = KEY_GENERATION_ERROR;
        return false;
    }

    if (!RAND_bytes(m_strInitialVector, iIVLength))
    {
        m_ErrorType = KEY_GENERATION_ERROR;
        return false;
    }

    m_ErrorType = NO_ERROR_REPORTED;
    return true;
}

bool CAesProxy::GetKey(unsigned char** strKey)
{
    //the caller must free the parameter pointer
    int iByteSize = GetByteSizeOfKey(GetCurrentEncryptionType());
    *strKey = new unsigned char[iByteSize];

    memset((char*) *strKey, 0, iByteSize);
    memcpy(*strKey, m_strKey, iByteSize);

    m_ErrorType = NO_ERROR_REPORTED;
    return true;
}

bool CAesProxy::GetInitialVector(unsigned char** strIV)
{
    //the caller must free the parameter pointer
    int iByteSize = GetByteSizeOfIV(GetCurrentEncryptionType());
    *strIV = new unsigned char[iByteSize];

    memset((char*) *strIV, 0, iByteSize);
    memcpy(*strIV, m_strInitialVector, iByteSize);

    m_ErrorType = NO_ERROR_REPORTED;
    return true;
}

/***
 * returns the number of bytes that a key must have
 * */
int CAesProxy::GetByteSizeOfKey(_ENC_TYPE encryptionType)
{
    if(encryptionType == AES128)
        return (int) KEY_LENGTH_AES128;
    else if(encryptionType == AES256)
        return (int) KEY_LENGTH_AES256;

    return 0;
}

/***
 * returns the number of bytes that an IV must have
 * */
int CAesProxy::GetByteSizeOfIV(_ENC_TYPE encryptionType)
{
    if(encryptionType == AES128)
        return (int) IV_LENGTH_AES128;
    else if(encryptionType == AES256)
        return (int) IV_LENGTH_AES256;

    return 0;
}

_ERROR_TYPE CAesProxy::GetLastError()
{
    return m_ErrorType;
}

//******************Private Methods******************//

void CAesProxy::ReleaseMemory()
{
    if(m_strKey != NULL)
        delete[] m_strKey;

    if(m_strInitialVector != NULL)
        delete[] m_strInitialVector;
}

bool CAesProxy::AllocateKeys()
{
    int iRet = true;
    ReleaseMemory();

    m_ErrorType = NO_ERROR_REPORTED;

    if( GetCurrentEncryptionType() == AES128 )
    {
        //AES-128 Key and Initial Vector (IV)
        m_strKey = new unsigned char[KEY_LENGTH_AES128 + 1];
        memset(m_strKey, 0, KEY_LENGTH_AES128 + 1);

        m_strInitialVector = new unsigned char[IV_LENGTH_AES128 + 1];
        memset(m_strInitialVector, 0, IV_LENGTH_AES128 + 1);
    }
    else if( GetCurrentEncryptionType() == AES256 )
    {
        //AES-256 Key and Initial Vector (IV)
        m_strKey = new unsigned char[KEY_LENGTH_AES256 + 1];
        memset(m_strKey, 0, KEY_LENGTH_AES256 + 1);

        m_strInitialVector = new unsigned char[IV_LENGTH_AES256 + 1];
        memset(m_strInitialVector, 0, IV_LENGTH_AES256 + 1);
    }
    else
    {
        //unknown type
        iRet = false;
        m_ErrorType = ENCRYPTION_TYPE_NOT_DEFINED;
    }

    return iRet;
}

bool CAesProxy::Encrypt(unsigned const char* const strPlainText, unsigned char** strCypheredText, unsigned int* iCypheredTextLength)
{
    m_ErrorType = NO_ERROR_REPORTED;

    if(strPlainText == NULL)
    {
        m_ErrorType = INVALID_PARAMETER;
        return false;
    }

    if(m_strKey == NULL || m_strInitialVector == NULL)
    {
        //if no key was generated, generate keys and
        //perform default encrytion onAES128
        m_ErrorType = NO_KEY_DEFINED;
        if(!GenerateKeys())
        {
            return false; //something bad happened
        }
    }

    /* Initialize the library */
//    ERR_load_crypto_strings();
//    OpenSSL_add_all_algorithms();
//    OPENSSL_config(NULL);

    unsigned int iPlainTextLength = strlen((char*)strPlainText);

    *strCypheredText = new unsigned char[iPlainTextLength + AES_BLOCK_SIZE];
    *iCypheredTextLength = iPlainTextLength + AES_BLOCK_SIZE;

    memset((char*) *strCypheredText, 0, *iCypheredTextLength);

    //Encrypt using symetric keys
    EVP_CIPHER_CTX* e_ctx;
    if (!(e_ctx = EVP_CIPHER_CTX_new()))
    {
        m_ErrorType = ENCRYPTION_ERROR;
        return false;
    }

    int outlen1, outlen2;

    if(GetCurrentEncryptionType() == AES128)
    {
        if(1 != EVP_EncryptInit_ex(e_ctx, EVP_aes_128_cbc(), NULL, m_strKey, m_strInitialVector) )
        {
            m_ErrorType = ENCRYPTION_ERROR;
            return false;
        }
    }
    else if (GetCurrentEncryptionType() == AES256)
    {
        /* Initialize key and IV */
        if(1 != EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, m_strKey, m_strInitialVector))
        {
            m_ErrorType = ENCRYPTION_ERROR;
            return false;
        }
    }

    if(1 != EVP_EncryptUpdate(e_ctx, *strCypheredText, &outlen1, strPlainText, iPlainTextLength) )
    {
        m_ErrorType = ENCRYPTION_ERROR;
        return false;
    }

    //Finalize the encryption. Further ciphertext bytes may be written at
    //this stage.
    if(1 != EVP_EncryptFinal_ex(e_ctx, *strCypheredText + outlen1, &outlen2))
    {
        m_ErrorType = ENCRYPTION_ERROR;
        return false;
    }

    *iCypheredTextLength = outlen1 + outlen2;

    //Clean up
    EVP_CIPHER_CTX_free(e_ctx);
//    EVP_cleanup();
//    ERR_free_strings();

    return true;
}

bool CAesProxy::Decrypt(unsigned const char* const strCypheredText, unsigned int iCypheredTextLength,
                       unsigned char** strPlainText, unsigned int* iPlainTextLength)
{
    m_ErrorType = NO_ERROR_REPORTED;

    if(strCypheredText == NULL)
    {
        m_ErrorType = INVALID_PARAMETER;
        return false;
    }

    if(m_strKey == NULL || m_strInitialVector == NULL)
    {
        //if no key was generated, the text cannot be decrypted
        m_ErrorType = NO_KEY_DEFINED;
        return false; //something bad happened
    }

//    //Initialize the library
//    ERR_load_crypto_strings();
//    OpenSSL_add_all_algorithms();
//    OPENSSL_config(NULL);

    //Allocate return variable
    *strPlainText = new unsigned char[iCypheredTextLength + AES_BLOCK_SIZE];
    *iPlainTextLength = iCypheredTextLength + AES_BLOCK_SIZE;

    memset((char*) *strPlainText, 0, *iPlainTextLength);

    //Encrypt using symetric keys
    EVP_CIPHER_CTX* e_ctx;
    if (!(e_ctx = EVP_CIPHER_CTX_new()))
    {
        m_ErrorType = DECRYPTION_ERROR;
        return false;
    }

    int outlen1, outlen2;

    if(GetCurrentEncryptionType() == AES128)
    {
        if(1 != EVP_DecryptInit_ex(e_ctx, EVP_aes_128_cbc(), NULL, m_strKey, m_strInitialVector) )
        {
            m_ErrorType = DECRYPTION_ERROR;
            return false;
        }
    }
    else if (GetCurrentEncryptionType() == AES256)
    {
        //Initialize key and IV
        if(1 != EVP_DecryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, m_strKey, m_strInitialVector))
        {
            m_ErrorType = DECRYPTION_ERROR;
            return false;
        }
    }

    //Provide the message to be decrypted, and obtain the plaintext output.
    //EVP_DecryptUpdate can be called multiple times if necessary
    if(1 != EVP_DecryptUpdate(e_ctx, *strPlainText, &outlen1, strCypheredText, iCypheredTextLength) )
    {
        m_ErrorType = DECRYPTION_ERROR;
        return false;
    }

    //Finalize the encryption. Further ciphertext bytes may be written at
    //this stage.
    if(1 != EVP_DecryptFinal_ex(e_ctx, *strPlainText + outlen1, &outlen2))
    {
        m_ErrorType = DECRYPTION_ERROR;
        return false;
    }

    *iPlainTextLength = outlen1 + outlen2;

    //Clean up
     EVP_CIPHER_CTX_free(e_ctx);
//     EVP_cleanup();
//     ERR_free_strings();

    return true;
}
