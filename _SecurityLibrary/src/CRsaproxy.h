#ifndef CRSAPROXY_H
#define CRSAPROXY_H

#include <string.h>
#include <iostream>
#include <ctime>

#include <openssl/rsa.h>
#include <openssl/pem.h>

using namespace std;

enum _RSA_KEY_TYPE{
    PRIVATE_KEY,
    PUBLIC_KEY
};

enum _RSA_ERROR_TYPE
{
    RSA_NO_ERROR,
    RSA_ENCRYPTION_ERROR,
    RSA_DECRYPTION_ERROR,
    RSA_INVALID_PARAMETER,
    RSA_KEY_TYPE_NOT_DEFINED,
    RSA_KEY_GENERATION_ERROR,
    RSA_MEMORY_ALLOCATION_ERROR,
    RSA_INSUFFICIENT_BUFFER_SIZE,
    RSA_OPENFILE_ERROR,
    RSA_NO_KEY_DEFINED
};

class CRsaProxy
{
public:
    CRsaProxy();
    ~CRsaProxy();

    bool GenerateKeys(bool bSaveToFile = false, char* strFolder = NULL, char* strPrivateKeyFile = NULL, char* strPublicKeyFile = NULL);

    bool SaveKeysOnFile(char* strFolder, char* strPrivateKeyFile, char* strPublicKeyFile);

    bool LoadKeysFromFile(char* strFileName, _RSA_KEY_TYPE enKeyType);

    _RSA_ERROR_TYPE GetLastError();

    int GetPrivateKeyLength();
    int GetPublicKeyLength();

    bool GetPrivateKey(char** strKey);
    bool GetPublicKey(char** strKey);

    bool Encrypt(_RSA_KEY_TYPE enKeyType, unsigned char* strPlainText, unsigned char** strCypheredText, unsigned int* iCypheredTextLength);
    bool Decrypt(_RSA_KEY_TYPE enKeyType, unsigned char* strCypheredText, unsigned int iCypheredTextLength, unsigned char** strPlainText, unsigned int* iPlainTextLength);

private:
    bool SetupKey(RSA* rsa, _RSA_KEY_TYPE enKeyType);
    void ReleaseMemory();
    RSA* CreateRSA(char* strKey, _RSA_KEY_TYPE enKeyType);

    int m_iPrivateKeyLength;
    int m_iPublicKeyLength;

    char* m_strPrivateKey;
    char* m_strPublicKey;
    _RSA_ERROR_TYPE m_ErrorType;
};

#endif // CRSAPROXY_H
