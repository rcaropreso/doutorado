#include "CRsaUnitTest.h"

CRsaUnitTest::CRsaUnitTest()
{
    m_LogType = OUT_SCREEN;
    m_fpLogFile = NULL;
}

CRsaUnitTest::CRsaUnitTest(string strFileName)
{
    m_LogType = OUT_FILE;
    m_fpLogFile = fopen(strFileName.c_str(), "w");

    if(m_fpLogFile == NULL)
    {
        cout << "Error opening log file!" << endl;
    }
}

CRsaUnitTest::~CRsaUnitTest()
{
    if(m_fpLogFile != NULL)
    {
        fclose(m_fpLogFile);
    }
}

void CRsaUnitTest::RunRSATest()
{
    CRsaProxy* oRSA = new CRsaProxy();

    printf("\n1 - RunRSAKeysTest");

    RunRSAKeysTest(oRSA);

    printf("\n2 - RunRSALoadKeysTest");

    RunRSALoadKeysTest(oRSA);

    printf("\n3 - RunRSAEncryptionTest");

    RunRSAEncryptionTest(oRSA);

    printf("\n4 - RunRSAEncryptionTest2 test");

    RunRSAEncryptionTest2(oRSA);

    delete oRSA;
}

bool CRsaUnitTest::RunRSAKeysTest(CRsaProxy* oRSA)
{
    stringstream ssMsg;

    //AES Test - BEGIN

    ssMsg << "Starting RSA tests..." << endl;

    //*****Key Generation RSA*****
    ssMsg << "Generating RSA Keys..." << endl;

    if(!oRSA->GenerateKeys())
    {
        ssMsg << "Error on RSA operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Send keys to Screen
    int iKeyLength = oRSA->GetPublicKeyLength();
    char* strKey = NULL;
    if(!oRSA->GetPublicKey(&strKey))
    {
        ssMsg << "Error on RSA - GetPublicKey() operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    ssMsg << "Public Key for RSA is " << iKeyLength << " bytes: " << endl << strKey << endl;

    delete[] strKey;
    strKey = NULL;
    iKeyLength = 0;

    iKeyLength = oRSA->GetPrivateKeyLength();
    if(!oRSA->GetPrivateKey(&strKey))
    {
        ssMsg << "Error on RSA - GetPrivateKey() operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    ssMsg << "Private Key for RSA is " << iKeyLength << " bytes: " << endl << strKey << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strKey;

    return true;
}

bool CRsaUnitTest::RunRSALoadKeysTest(CRsaProxy* oRSA)
{
    stringstream ssMsg;

    ssMsg << "Saving RSA Keys on files..." << endl;

    char strPrivateKeyFile[256];
    char strPublicKeyFile[256];

    if(!oRSA->SaveKeysOnFile((char*)"", strPrivateKeyFile, strPublicKeyFile))
    {
        ssMsg << "Error on RSA - SaveKeysOnFile() operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    ssMsg << "Loading RSA Keys from files..." << endl;

    if(!oRSA->LoadKeysFromFile(strPrivateKeyFile, PRIVATE_KEY))
    {
        ssMsg << "Error on RSA - LoadKeysFromFile(PrivateKey) operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    if(!oRSA->LoadKeysFromFile(strPublicKeyFile, PUBLIC_KEY))
    {
        ssMsg << "Error on RSA - LoadKeysFromFile(Public) operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Send keys to Screen
    int iKeyLength = oRSA->GetPublicKeyLength();
    char* strKey = NULL;
    if(!oRSA->GetPublicKey(&strKey))
    {
        ssMsg << "Error on RSA - GetPublicKey() operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    ssMsg << "Public Key for RSA is " << iKeyLength << " bytes: " << endl << strKey << endl;

    delete[] strKey;
    strKey = NULL;
    iKeyLength = 0;

    iKeyLength = oRSA->GetPrivateKeyLength();
    if(!oRSA->GetPrivateKey(&strKey))
    {
        ssMsg << "Error on RSA - GetPrivateKey() operation. Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    ssMsg << "Private Key for RSA is " << iKeyLength << " bytes: " << endl << strKey << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strKey;

    return true;
}

bool CRsaUnitTest::RunRSAEncryptionTest(CRsaProxy* oRSA)
{
    //Public Encryption X Private Decryption
    unsigned char strPlainText[8] = "Rodrigo";

    stringstream ssMsg;

    ssMsg << "\nEncryption test (RSA Pub key) for \'Rodrigo\'..." << endl;

    //*****Encryption Test RSA Public Encryption*****
    unsigned char* strCypheredText = NULL;
    unsigned int iCypheredTextLength = 0;

    if(!oRSA->Encrypt(PUBLIC_KEY, strPlainText, &strCypheredText, &iCypheredTextLength)) //strPlainText contains 'Rodrigo' initialized on the begginning of the function
    {
        ssMsg << "Encryption test (RSA Pub) for \'Rodrigo\' FAILED! Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Base 64
//    QString strAux;
//    QByteArray ba((char*)strCypheredText, iCypheredTextLength);
//    strAux = QString("Cyphered text for RSA Pub Key (Base64) is %1 bytes: [%2]").arg(iCypheredTextLength).arg(QString(ba.toBase64()));
//    txtDebug->append(strAux);

    string sText;
    char strAux[3];
    for(unsigned int i=0; i < iCypheredTextLength; i++)
    {
        sprintf(strAux, "%02x", strCypheredText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nCyphered text for RSA Pub Key is " << iCypheredTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    //*****Decryption Test RSA*****
    ssMsg << "\nDecryption test (RSA Priv Key) for '" << sText << "'..." << endl;

    unsigned char* strDecodedText = NULL;
    unsigned int iDecodedTextLength = 0;
    if(!oRSA->Decrypt(PRIVATE_KEY, strCypheredText, iCypheredTextLength, &strDecodedText, &iDecodedTextLength))
    {
        ssMsg << "Decryption test (RSA Priv Key) for \'Rodrigo\' FAILED! Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);
    for(unsigned int i=0; i < iDecodedTextLength; i++)
    {
        sprintf(strAux, "%c", strDecodedText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nDecoded text for RSA(Priv Key) is " << iDecodedTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strCypheredText;
    delete[] strDecodedText;

    return true;
}

bool CRsaUnitTest::RunRSAEncryptionTest2(CRsaProxy* oRSA)
{
    //Private Encryption X Public Decryption
    unsigned char strPlainText[8] = "Rodrigo";

    stringstream ssMsg;

    ssMsg << "\nEncryption test (RSA Priv key) for \'Rodrigo\'..." << endl;

    //*****Encryption Test RSA Private Encryption*****
    unsigned char* strCypheredText = NULL;
    unsigned int iCypheredTextLength = 0;

    if(!oRSA->Encrypt(PRIVATE_KEY, strPlainText, &strCypheredText, &iCypheredTextLength)) //strPlainText contains 'Rodrigo' initialized on the begginning of the function
    {
        ssMsg << "Encryption test (RSA Priv) for \'Rodrigo\' FAILED! Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    //Base 64
//    QString strAux;
//    QByteArray ba((char*)strCypheredText, iCypheredTextLength);
//    strAux = QString("Cyphered text for RSA Pub Key (Base64) is %1 bytes: [%2]").arg(iCypheredTextLength).arg(QString(ba.toBase64()));
//    txtDebug->append(strAux);

    string sText;
    char strAux[3];
    for(unsigned int i=0; i < iCypheredTextLength; i++)
    {
        sprintf(strAux, "%02x", strCypheredText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nCyphered text for RSA Priv Key is " << iCypheredTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    ssMsg.str("");
    ssMsg.clear();

    //*****Decryption Test RSA*****
    ssMsg << "\nDecryption test (RSA Pub Key) for '" << sText << "'..." << endl;

    unsigned char* strDecodedText = NULL;
    unsigned int iDecodedTextLength = 0;
    if(!oRSA->Decrypt(PUBLIC_KEY, strCypheredText, iCypheredTextLength, &strDecodedText, &iDecodedTextLength))
    {
        ssMsg << "Decryption test (RSA Pub Key) for \'Rodrigo\' FAILED! Error Code [" << oRSA->GetLastError() << "]" << endl;
        LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());
        return false;
    }

    sText.clear();
    memset(strAux, 0, 3);
    for(unsigned int i=0; i < iDecodedTextLength; i++)
    {
        sprintf(strAux, "%c", strDecodedText[i]);
        sText.append(strAux);
    }

    ssMsg << "\nDecoded text for RSA(Pub Key) is " << iDecodedTextLength << " bytes: " << endl << sText << endl;
    LogOutput((char*)ssMsg.str().c_str(), ssMsg.str().length());

    delete[] strCypheredText;
    delete[] strDecodedText;

    return true;
}

void CRsaUnitTest::LogOutput(char* strMessage, int iMessageLength)
{
    if(m_LogType == OUT_SCREEN)
    {
        cout << strMessage << endl;
    }
    else if(m_LogType == OUT_FILE)
    {
        fwrite(strMessage, 1, iMessageLength, m_fpLogFile);
    }
}
