#include "Complexo.h"
 
Complexo::Complexo()
{
	real = imag = 0;
}  
 
Complexo::Complexo(double real, double imag)
{
  	this->real = real;
 	this->imag = imag;
} 
 
void Complexo::setReal(double real)
{ 
	this->real = real;
}

void Complexo::setImag(double imag)
{ 
	this->imag = imag;
}

double Complexo::getReal()
{ 
	return this->real;
}
 
double Complexo::getImag()
{ 
	return this->imag;
} 
 
void Complexo::soma(Complexo *c2)
{
 	this->real += c2->real;
	this->imag += c2->imag;
}

void Complexo::subtracao(Complexo *c2)
{
 	this->real = c2->real - this->real;
	this->imag = c2->imag - this->imag;
}

void Complexo::multiplicacao(Complexo *c2, Complexo *c3)
{ 
	
	c3->real = this->real * c2->real - this->imag * c2->imag;
	c3->imag = this->real * c2->imag + this->imag * c2->real;
	
}

double Complexo::abs()
{ 
	return sqrt((this->real * this->real) + (this->imag * this->imag));
} 
 



