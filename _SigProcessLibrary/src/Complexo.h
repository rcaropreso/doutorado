#ifndef COMPLEXO_H_
#define COMPLEXO_H_

#include <iostream>
#include <math.h> 
 
using namespace std;
 
 class Complexo 
 {
 	private:
		double real; 
		double imag;
	public:
		Complexo();
		Complexo(double real, double imag);
		void soma(Complexo *c2);
		void subtracao(Complexo *c2);
		void multiplicacao(Complexo *c2,Complexo *c3);
		void setReal(double real);
		void setImag(double imag);
		double getReal();
		double getImag();
		double abs();
 };
 
#endif /* COMPLEXO_H_ */
