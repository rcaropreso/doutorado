/*
 * CParseCmd.h
 *
 *  Created on: 31/05/2014
 *      Author: Rodrigo
 */

#ifndef CPARSECMD_H_
#define CPARSECMD_H_

#include <iostream>
#include <map>

#define INI_FILE_ATTRIB   "-f"
#define INI_IP_ATTRIB     "-i"
#define INI_PORT_ATTRIB   "-p"
#define INI_SAMPLE_TIME   "-t"
#define INI_SECURITY_MODE "-sec"

enum ParameterID
{
	ID_INI_FILE,
	ID_IP_ADDRESS,
	ID_PORT_NUMBER,
	ID_TIME_SAMPLE,  //time, in seconds for the PRU sampling
	ID_SECURITY_MODE //indicates if the security will be used or not
};

class CParseCmd
{
public:
	CParseCmd();
	virtual ~CParseCmd();

	bool ParseCmd(int argc, char *argv[]);

	std::string GetParameter(ParameterID idParam);

private:
	std::map<ParameterID, std::string> m_mapParameters;
};

#endif /* CPARSECMD_H_ */
