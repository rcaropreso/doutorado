#include "Fourier.h"

Fourier::Fourier(int N, double *vetor)
{
	
	
	Npontos = N;
	this->vetorComplexo = new Complexo[Npontos];
	
	for(int i = 0; i < this->Npontos; i++){
		
		vetorComplexo[i].setReal(vetor[i]);
		vetorComplexo[i].setImag(0); 
	}	
	 
}

Fourier::~Fourier(){
	delete [] vetorComplexo;
}

void Fourier::troca(Complexo *c1, Complexo *c2) {
	Complexo tmp;
	tmp = *c1;
	*c1 = *c2;
	*c2 = tmp;
}


/* re-ordenacao da entrada */ 
void Fourier::reord() {
	int dir, inv, pot2;
	inv = 0;
	for (dir = 1; dir < Npontos; dir++) {
		/* incrementa contador de  ordem inversa dos bits*/
		for (pot2 = Npontos / 2; pot2 <= inv; pot2 /= 2)
			inv -= pot2;
		inv += pot2;
		if (dir < inv)
			troca(&vetorComplexo[dir], &vetorComplexo[inv]);
	}


}


/*  Calcula  a  tabela  de  factores  multiplicativos  */
void Fourier::factores() {
	double arg = 1.5707963;
	/* 	 pi/2 	 */
	int m;
	Log2N = (int) (log(Npontos + .1) / log(2.));
	Wr[0].setReal(1);
	Wr[0].setImag(0);
	Wr[1].setReal(0);
	Wr[1].setImag(-1);
	for (m = 2; m < Log2N; m++) {
		arg /= 2.;
		Wr[m].setReal((double) cos(arg));
		Wr[m].setImag((double) -sin(arg));
	}

}

/*  primeiro passo (m=1): DFTs de 2 Npontos */
void Fourier::dft2() {
	int p = 0;
	Complexo temp;
	/* q = p + 1 * Xm(p) = Xm-1(p) + Xm-1(q) * Xm(q) = Xm-1(p) - Xm-1(q) */
	for (p = 0; p < Npontos; p += 2) {
		temp = vetorComplexo[p + 1];
		vetorComplexo[p + 1].subtracao(&vetorComplexo[p]);
		vetorComplexo[p].soma(&temp);
	
	}

}


/* borboletas de m=2 a log2(N) */
void Fourier::borbol() {
	Complexo temp, Wm;
	int m, j, p, potm2 = 2, potm22 = 4;
	for (m = 2; m <= Log2N; m++) {
		Wm = Wr[0];
		for (j = 0; j < potm2; j++) { /* potm2 = 2�(m-1) */
			for (p = j; p < Npontos; p += potm22) { /*potm2 = 2�m	 */
				vetorComplexo[p + potm2].multiplicacao(&Wm,&temp);
				
				vetorComplexo[p + potm2].setReal(vetorComplexo[p].getReal() - temp.getReal());
				vetorComplexo[p + potm2].setImag(vetorComplexo[p].getImag() - temp.getImag());
				vetorComplexo[p].soma(&temp);
					
			}
			Wm.multiplicacao(&Wr[m-1],&temp);
			Wm = temp;
						
		}
		potm2 = potm22;
		potm22 *= 2;
	}
	

}

/*  algoritmo fft de N Npontos com decimacao no tempo */
void Fourier::fft(double *saida){

	static int Nanterior = 0;
	reord();
	if (Npontos != Nanterior) {
		Nanterior = Npontos;
		factores();
	}
	dft2();
	borbol();
	
	
	for (int i = 0; i < Npontos/2; i++){
	
	
		saida[i] = 2*((vetorComplexo[i].abs())/this->Npontos);
		
	}
	
	
}



void Fourier::imprimir(){
	for (int i = 0; i < Npontos; i++){
		cout << vetorComplexo[i].getReal() << "\n";
	}	
}
