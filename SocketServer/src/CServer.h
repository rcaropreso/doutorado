/*
 * CServer.h
 *
 *  Created on: 01/05/2014
 *  Author: Rodrigo de T. Caropreso
 */

#ifndef CSERVER_H_
#define CSERVER_H_

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#ifdef __WINDOWS
	#include <process.h>
	#include <windows.h>
	#include "winsock2.h"
    #include <pthread.h>
#endif

#ifdef __LINUX
#include <pthread.h>
#endif

using namespace std;

#ifdef __LINUX
	typedef unsigned int SOCKET;
#endif

class CServer
{
public:
	CServer(string sIP, long lPort);
	virtual ~CServer();

	bool StartListen();
	void StopListen();

	SOCKET WaitForConnection(SOCKET sock);
	SOCKET GetHandle();

private:
	string m_sIPAddress;
	long m_lPortNumber; //listen port
	SOCKET m_uSocket;
	bool m_bSocketOpened;

	bool IsOpen(){return m_bSocketOpened;};

	void HandleAcceptedConnection( SOCKET sock );

	static void* threadConnection(void *obj);

	void DoWork();

	vector<void*> m_vThreads;

	bool StartListenForWindows();
	bool StartListenForLinux();
};

#endif /* CSERVER_H_ */
